#include "ScaleFilter.h"
#include "Canvas2D.h"
#include <math.h>

ScaleFilter::ScaleFilter(Canvas2D *canvas)
    : Filter(canvas)
{

}

void ScaleFilter::scanImage()
{
    // Only scale if the scale factor is != 1.
    // First scale in the x-direction.
//    if (settings.scaleX != 1.f) {
        scaleX(settings.scaleX);
//    }

    // Then scale in the y-direction.
//    if (settings.scaleY != 1.f) {
        scaleY(settings.scaleY);
//    }
}

void ScaleFilter::scaleX(float scaleFactor)
{
    BGRA* pix = m_canvas->data();
    int width = m_canvas->width();
    int height = m_canvas->height();

    // Forward declarations
    int rowstart, rowend, rowcounter, colstart, colend, colcounter, newIndex, oldIndex;

    float hPrime, sumR, sumG, sumB, supportWidth, weight, newWidth;

    // Set support width to either 2 or 2 / scaleFactor if the scale factor is greater than 1 or less than one respectively.
    supportWidth = (scaleFactor < 1) ? (2.f / scaleFactor) : 2.f;

    // No user selection, apply filter to the entire image.
    if (m_canvas->marqueeStart().x() == m_canvas->marqueeStop().x() &&
            m_canvas->marqueeStart().y() == m_canvas->marqueeStop().y()) {
        newWidth = width * scaleFactor;

        rowstart = 0;
        rowend = height;
        colstart = 0;
        colend = newWidth;

    }
    // Check for edge case (for when stop < start);
    else {
        rowstart = max(min(m_canvas->marqueeStart().y(), m_canvas->marqueeStop().y()), 0);
        rowend = min(max(m_canvas->marqueeStop().y(), m_canvas->marqueeStart().y()), m_canvas->height());
        colstart = max(min(m_canvas->marqueeStart().x(), m_canvas->marqueeStop().x()), 0);
        colend = min(max(m_canvas->marqueeStop().x(), m_canvas->marqueeStart().x()), m_canvas->width());

        // New Width and Height
        newWidth = (colend - colstart) * scaleFactor;
        height = rowend - rowstart;
    }

    std::vector<glm::vec3> tempNewPix;
    tempNewPix.reserve(newWidth * height);

    for (rowcounter = 0; rowcounter < height; rowcounter++) {
        for (colcounter = 0; colcounter < newWidth; colcounter++) {

            hPrime = backMapping(colcounter, scaleFactor);
            sumR = 0.f;
            sumG = 0.f;
            sumB = 0.f;

            for (int k = ceil(hPrime - supportWidth); k <= floor(hPrime + supportWidth); k++) {
                weight = ((supportWidth - abs(hPrime - k)) / supportWidth);
                // TODO: Take into account height here for y scaling.
                oldIndex = glm::clamp(k + colstart, 0, width - 1);
                oldIndex = get1DIndex(width, oldIndex, rowcounter + rowstart);

                sumR += pix[oldIndex].r * weight;
                sumG += pix[oldIndex].g * weight;
                sumB += pix[oldIndex].b * weight;

            }

            // Index of new canvas.
            // keep into acount scaling in the y-direction; pass back
            // column instead of individual index.
            newIndex = get1DIndex(newWidth, colcounter, rowcounter);
            tempNewPix[newIndex] = glm::vec3(sumR / supportWidth, sumG / supportWidth, sumB / supportWidth);

        }
    }

    // Resize the canvas and put in the new data.
    m_canvas->resize(newWidth, height);
    pix = m_canvas->data();
    for (rowcounter = 0; rowcounter < height; rowcounter++) {
        for (colcounter = 0; colcounter < newWidth; colcounter++) {
            newIndex = get1DIndex(newWidth, colcounter, rowcounter);
            pix[newIndex].r = tempNewPix[newIndex].x;
            pix[newIndex].g = tempNewPix[newIndex].y;
            pix[newIndex].b = tempNewPix[newIndex].z;
        }
    }
}


// Scale images in the y factor.
void ScaleFilter::scaleY(float scaleFactor)
{
    BGRA* pix = m_canvas->data();
    int width = m_canvas->width();
    int height = m_canvas->height();

    // Forward declarations
    int rowstart, rowend, rowcounter, colstart, colend, colcounter, newIndex, oldIndex;

    float hPrime, sumR, sumG, sumB, supportWidth, weight;

    // Set support width to either 2 or 2 / scaleFactor
    // if the scale factor is greater than 1 or less than
    // one respectively.
    supportWidth = (scaleFactor < 1) ? (2.f / scaleFactor) : 2.f;

    float newHeight = height * scaleFactor;

    std::vector<glm::vec3> tempNewPix;
    tempNewPix.reserve(width * newHeight);

    // No user selection, apply filter to the entire image.
    if (m_canvas->marqueeStart().x() == m_canvas->marqueeStop().x() &&
            m_canvas->marqueeStart().y() == m_canvas->marqueeStop().y()) {
        rowstart = 0;
        rowend = newHeight;
        colstart = 0;
        colend = width;
    }
    // Check for edge case (for when stop < start);
    else {
        rowstart = max(min(m_canvas->marqueeStart().y(), m_canvas->marqueeStop().y()), 0);
        rowend = min(max(m_canvas->marqueeStop().y(), m_canvas->marqueeStart().y()), m_canvas->height());
        colstart = max(min(m_canvas->marqueeStart().x(), m_canvas->marqueeStop().x()), 0);
        colend = min(max(m_canvas->marqueeStop().x(), m_canvas->marqueeStart().x()), m_canvas->width());
    }

    for (colcounter = colstart; colcounter < colend; colcounter++) {
        for (rowcounter = rowstart; rowcounter < rowend; rowcounter++) {

            hPrime = backMapping(rowcounter, scaleFactor);
            sumR = 0.f;
            sumG = 0.f;
            sumB = 0.f;

            for (int k = ceil(hPrime - supportWidth); k <= floor(hPrime + supportWidth); k++) {
                weight = ((supportWidth - abs(hPrime - k)) / supportWidth);
                // TODO: Take into account height here for y scaling.
                oldIndex = glm::clamp(k, 0, height - 1);
                oldIndex = get1DIndex(width, colcounter, oldIndex);
                sumR += pix[oldIndex].r * weight;
                sumG += pix[oldIndex].g * weight;
                sumB += pix[oldIndex].b * weight;

            }

            // Index of new canvas.
            // keep into acount scaling in the y-direction; pass back
            // column instead of individual index.
            newIndex = get1DIndex(width, colcounter, rowcounter);
            tempNewPix[newIndex] = glm::vec3(sumR / supportWidth, sumG / supportWidth, sumB / supportWidth);

        }
    }

    // Resize the canvas and put in the new data.
    m_canvas->resize(width, newHeight);
    pix = m_canvas->data();
    for (rowcounter = rowstart; rowcounter < rowend; rowcounter++) {
        for (colcounter = colstart; colcounter < colend; colcounter++) {
            newIndex = get1DIndex(width, colcounter, rowcounter);
            pix[newIndex].r = tempNewPix[newIndex].x;
            pix[newIndex].g = tempNewPix[newIndex].y;
            pix[newIndex].b = tempNewPix[newIndex].z;
        }
    }
}

// Function that backmaps destination index to
// source index. Takes in a scale factor (in either the x or
// y direction). Returns hPrime.
float ScaleFilter::backMapping(int index, float factor)
{
    return (index / factor) + ((1 - factor) / (2 * factor));
}
