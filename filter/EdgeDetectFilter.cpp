#include "EdgeDetectFilter.h"
#include "Canvas2D.h"
#include <math.h>

EdgeDetectFilter::EdgeDetectFilter(Canvas2D *canvas)
    : Filter(canvas)
{

}

// Overwritten to use edge detect function.
void EdgeDetectFilter::scanImage()
{
    edgeDetect();
}

// Applies an edge detect filter to the image. First convolves in the to store Gx/Gytemp values, storing
// calculated energy values in the m_tempPix vector (vec2 to store both values
// per index. Then convolve once more the perform the final calculations for G.
// Update the actual pixel data with the newly calculated energy values.
void EdgeDetectFilter::edgeDetect()
{    
    BGRA* pix = m_canvas->data();
    int width = m_canvas->width();
    int height = m_canvas->height();

    m_tempPix.reserve(2 * width * height);

    // Forward declarations
    glm::vec3 horizPixels, vertPixels;

    int rowstart, rowend, rowcounter, colstart, colend, colcounter, index,
            left, leftIndex, right, rightIndex, top, topIndex, bottom, bottomIndex;

    float GxTemp, GyTemp, Gx, Gy, G;

    glm::vec3 v = glm::vec3(1.f, 2.f, 1.f);
    glm::vec3 h = glm::vec3(-1.f ,0.f, 1.f);
    glm::vec3 flippedh = glm::vec3(1.f, 0.f, -1.f);

    // No user selection, apply filter to the entire image.
    if (m_canvas->marqueeStart().x() == m_canvas->marqueeStop().x() &&
            m_canvas->marqueeStart().y() == m_canvas->marqueeStop().y()) {
        rowstart = 0;
        rowend = height;
        colstart = 0;
        colend = width;
    }
    // Check for edge case (for when stop < start);
    else {
        rowstart = max(min(m_canvas->marqueeStart().y(), m_canvas->marqueeStop().y()), 0);
        rowend = min(max(m_canvas->marqueeStop().y(), m_canvas->marqueeStart().y()), m_canvas->height());
        colstart = max(min(m_canvas->marqueeStart().x(), m_canvas->marqueeStop().x()), 0);
        colend = min(max(m_canvas->marqueeStop().x(), m_canvas->marqueeStart().x()), m_canvas->width());
    }

    // First scan
    for (rowcounter = rowstart; rowcounter < rowend; rowcounter++) {
        for (colcounter = colstart; colcounter < colend; colcounter++) {
            // Use clamp to extend pixels when necessary (edge indices).
            left = glm::clamp(colcounter - 1, 0, width - 1);
            right = glm::clamp(colcounter + 1, 0, width - 1);
            top = glm::clamp(rowcounter - 1, 0, height - 1);
            bottom = glm::clamp(rowcounter + 1, 0, height - 1);

            // Calculate indices to grab proper pixels for edge detection.
            leftIndex = get1DIndex(width, left, rowcounter);
            rightIndex = get1DIndex(width, right, rowcounter);
            topIndex = get1DIndex(width, colcounter, top);
            bottomIndex = get1DIndex(width, colcounter, bottom);

            // Index of current pixel.
            index = get1DIndex(width, colcounter, rowcounter);

            // Horizontal and vertical rows/strips of pixel values for current index.
            horizPixels = glm::vec3(pix[leftIndex].r, pix[index].r, pix[rightIndex].r);
            vertPixels = glm::vec3(pix[topIndex].r, pix[index].r, pix[bottomIndex].r);

            // Calculating through first loop.
            // store this in a an array to loop through.
            GxTemp = glm::dot(horizPixels, h);
            GyTemp = glm::dot(vertPixels, v);

            // Store GxTemp and GyTemp in m_tempPix at the corresponding index.
            // Stored as a vec2 to store both values.
            m_tempPix[index] = glm::vec2(GxTemp, GyTemp);
        }
    }

    // Second scan
    for (rowcounter = rowstart; rowcounter < rowend; rowcounter++) {
        for (colcounter = colstart; colcounter < colend; colcounter++) {
            // Use clamp to extend pixels when necessary (edge indices).
            left = glm::clamp(colcounter - 1, 0, width - 1);
            right = glm::clamp(colcounter + 1, 0, width - 1);
            top = glm::clamp(rowcounter - 1, 0, height - 1);
            bottom = glm::clamp(rowcounter + 1, 0, height - 1);

            // Calculate indices to grab proper pixels for edge detection.
            leftIndex = get1DIndex(width, left, rowcounter);
            rightIndex = get1DIndex(width, right, rowcounter);
            topIndex = get1DIndex(width, colcounter, top);
            bottomIndex = get1DIndex(width, colcounter, bottom);

            // Index of current pixel.
            index = get1DIndex(width, colcounter, rowcounter);

            // Horizontal and vertical rows/strips of pixel values for current index (now
            // using m_tempPix values for second pass).
            horizPixels = glm::vec3(m_tempPix[leftIndex].x, m_tempPix[index].x, m_tempPix[rightIndex].x);
            vertPixels = glm::vec3(m_tempPix[topIndex].y, m_tempPix[index].y, m_tempPix[bottomIndex].y);

            // Calculating through first loop.
            // store this in a an array to loop through.
            Gx = glm::dot(horizPixels, v);
            Gy = glm::dot(vertPixels, flippedh);

            // Calculate value of G.
            G = sqrt(pow(Gx, 2) + pow(Gy, 2));

            // Update pixel data with newly calculated sums.
            pix[index].r = G * settings.edgeDetectThreshold;
            pix[index].g = G * settings.edgeDetectThreshold;
            pix[index].b = G * settings.edgeDetectThreshold;
        }
    }
}
