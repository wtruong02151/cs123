#include "GrayscaleFilter.h"
#include "Canvas2D.h"

GrayscaleFilter::GrayscaleFilter(Canvas2D *canvas)
    : Filter(canvas)
{

}

// For a given pixel, perform the calculations the
// grayscale it.
void GrayscaleFilter::applyFilter(BGRA *imageData, int index)
{
    float gray =  .299 * imageData[index].r + .587 * imageData[index].g + .114 * imageData[index].b;
    imageData[index].r = gray;
    imageData[index].g = gray;
    imageData[index].b = gray;
}
