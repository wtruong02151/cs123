#include "BlurFilter.h"
#include "Canvas2D.h"
#include <math.h>

BlurFilter::BlurFilter(Canvas2D *canvas)
    : Filter(canvas)
{
    m_radius = settings.blurRadius;
    makeKernel(1.f);
}

// Overwritten to use blur function.
void BlurFilter::scanImage()
{
    blur();
}

// Applies a blur filter to the image. First convolves in the x-direction, storing
// calculated energy values in the m_tempPix vector. Then convolves in the y-direction,
// updating the actual pixel data with the newly calculated energy values.
void BlurFilter::blur()
{
    BGRA* pix = m_canvas->data();
    int width = m_canvas->width();
    int height = m_canvas->height();

    // Reserve data up to the size of the canvas.
    m_tempPix.reserve(2 * width * height);

    // Forward declarations
    int rowstart, rowend, rowcounter, colstart, colend, colcounter, index,
            curr, curr1dIndex;

    float sumR, sumG, sumB;

    // No user selection, apply filter to the entire image.
    if (m_canvas->marqueeStart().x() == m_canvas->marqueeStop().x() &&
            m_canvas->marqueeStart().y() == m_canvas->marqueeStop().y()) {
        rowstart = 0;
        rowend = height;
        colstart = 0;
        colend = width;
    }
    // Check for edge case (for when stop < start);
    else {
        rowstart = max(min(m_canvas->marqueeStart().y(), m_canvas->marqueeStop().y()), 0);
        rowend = min(max(m_canvas->marqueeStop().y(), m_canvas->marqueeStart().y()), m_canvas->height());
        colstart = max(min(m_canvas->marqueeStart().x(), m_canvas->marqueeStop().x()), 0);
        colend = min(max(m_canvas->marqueeStop().x(), m_canvas->marqueeStart().x()), m_canvas->width());
    }

    // X-convolution
    for (rowcounter = rowstart; rowcounter < rowend; rowcounter++) {
        for (colcounter = colstart; colcounter < colend; colcounter++) {

            // Index of current pixel.
            index = get1DIndex(width, colcounter, rowcounter);
            sumR = 0.f;
            sumG = 0.f;
            sumB = 0.f;

            // Loop through kernel and calculate convolution values.
            for (int i = -m_radius; i <= m_radius; i++) {

                curr = glm::clamp(colcounter + i, 0, width - 1);
                curr1dIndex = get1DIndex(width, curr, rowcounter);

                sumR += m_kernel[i + m_radius] * pix[curr1dIndex].r;
                sumG += m_kernel[i + m_radius] * pix[curr1dIndex].g;
                sumB += m_kernel[i + m_radius] * pix[curr1dIndex].b;
            }
            // Store sum in temporary vector.
            m_tempPix[index] = glm::vec3(sumR, sumG, sumB);
        }
    }

    // Y-convolution
    for (rowcounter = rowstart; rowcounter < rowend; rowcounter++) {
        for (colcounter = colstart; colcounter < colend; colcounter++) {

            // Index of current pixel.
            index = get1DIndex(width, colcounter, rowcounter);
            sumR = 0.f;
            sumG = 0.f;
            sumB = 0.f;

            // Loop through kernel and calculate convolution values.
            for (int i = -m_radius; i <= m_radius; i++) {

                curr = glm::clamp(rowcounter + i, 0, height - 1);
                curr1dIndex = get1DIndex(width, colcounter, curr);

                sumR += m_kernel[i + m_radius] * m_tempPix[curr1dIndex].x;
                sumG += m_kernel[i + m_radius] * m_tempPix[curr1dIndex].y;
                sumB += m_kernel[i + m_radius] * m_tempPix[curr1dIndex].z;
            }

            // Update pixel data with newly calculated sums.
            pix[index].r = sumR;
            pix[index].g = sumG;
            pix[index].b = sumB;
        }
    }
}

// Function that creates the kernel used to blur the image for a
// given radius and sigma value.
void BlurFilter::makeKernel(int sigma)
{
    // Reserve proper amount of memory.
    m_kernel.reserve(2 * m_radius + 1);

    // Normalization component.
    float normalization = (1.0 / (sqrt(2.0 * M_PI) * sigma));

    // Iterate through kernel vector and fill in values.
    for (int i = -m_radius; i <= m_radius; i++) {
        float eVal = pow(exp(1.0), -(pow(i, 2.0) / (2.0 * pow(sigma, 2.0))));
        m_kernel[i + m_radius] = normalization * eVal;
    }
}
