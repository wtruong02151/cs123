#ifndef SHARPENFILTER_H
#define SHARPENFILTER_H

#include "Filter.h"
#include "Settings.h"

class SharpenFilter
    : public Filter
{
public:
    SharpenFilter(Canvas2D* canvas);

    virtual void scanImage();

private:
    // Function that convolves image and applies the sharpen filter.
    void sharpen();

    // Array that stores modified pixel values. Vector
    // of vec3's to store each BGR component of a pixel.
    std::vector<glm::vec3> m_tempPix;

    // Define member variable for kernel
    std::vector<float> m_kernel;
};

#endif // SHARPENFILTER_H
