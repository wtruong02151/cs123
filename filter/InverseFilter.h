#ifndef INVERSEFILTER_H
#define INVERSEFILTER_H

#include <CS123Common.h>
#include "Filter.h"

class InverseFilter
    : public Filter
{
public:
    InverseFilter(Canvas2D* canvas);

private:
    // Virtual function to be overwritten.
    virtual void applyFilter(BGRA* imageData, int index);
};

#endif // INVERSEFILTER_H
