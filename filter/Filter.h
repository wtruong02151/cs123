#ifndef FILTER_H
#define FILTER_H

#include <CS123Common.h>
#include "Settings.h"


class Canvas2D;

class Filter
{
public:
    // Takes in the canvas as a parameter.
    Filter(Canvas2D* canvas);

    // Function to scan image (for loops).
    virtual void scanImage();

protected:

    Canvas2D* m_canvas;

    // Function that, given 2D coordinates and the width of the 2D
    // array, returns the corresponding 1D index.
    int get1DIndex(int width, int col, int row);

    // Virtual function to be overwritten by subclasses.
    virtual void applyFilter(BGRA *imageData, int index);

};
#endif // FILTER_H
