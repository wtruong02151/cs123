#include "InverseFilter.h"
#include "Canvas2D.h"

InverseFilter::InverseFilter(Canvas2D *canvas)
    : Filter(canvas)
{

}

// For a given pixel, perform the calculations to
// inverse its pixel data.
void InverseFilter::applyFilter(BGRA *imageData, int index)
{
    imageData[index].r = 255 - imageData[index].r;
    imageData[index].g = 255 - imageData[index].g;
    imageData[index].b = 255 - imageData[index].b;
}
