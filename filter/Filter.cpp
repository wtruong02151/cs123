#include "filter/Filter.h"
#include "Canvas2D.h"

Filter::Filter(Canvas2D *canvas)
{
    m_canvas = canvas;
}

void Filter::scanImage()
{
    BGRA* pix = m_canvas->data();
    int rowstart, rowend, rowcounter, colstart, colend, colcounter, index;

    // No user selection, apply filter to the entire image.
    if (m_canvas->marqueeStart().x() == m_canvas->marqueeStop().x() &&
            m_canvas->marqueeStart().y() == m_canvas->marqueeStop().y()) {
        rowstart = 0;
        rowend = m_canvas->height();
        colstart = 0;
        colend = m_canvas->width();
    }
    // Check for edge case (for when stop < start);
    else {
        rowstart = max(min(m_canvas->marqueeStart().y(), m_canvas->marqueeStop().y()), 0);
        rowend = min(max(m_canvas->marqueeStop().y(), m_canvas->marqueeStart().y()), m_canvas->height());
        colstart = max(min(m_canvas->marqueeStart().x(), m_canvas->marqueeStop().x()), 0);
        colend = min(max(m_canvas->marqueeStop().x(), m_canvas->marqueeStart().x()), m_canvas->width());
    }

    for (rowcounter = rowstart; rowcounter < rowend; rowcounter++) {
        for (colcounter = colstart; colcounter < colend; colcounter++) {
            // Get 1D index.
            index = get1DIndex(m_canvas->width(), colcounter, rowcounter);

            // Call subclass's applyFilter function.
            applyFilter(pix, index);
        }
    }
}

// Function that converts a 2D index to a 1D index.
int Filter::get1DIndex(int width, int col, int row)
{
    return (width * row) + col;
}

// Empty function to be overwritten.
void Filter::applyFilter(BGRA *imageData, int index)
{

}
