#include "SharpenFilter.h"
#include "Canvas2D.h"
#include <math.h>

SharpenFilter::SharpenFilter(Canvas2D *canvas)
    : Filter(canvas)
{
    // Kernel used to sharpen image.
    m_kernel = { 0.f, -1.f, 0.f,
                 -1.f, 5.f, -1.f,
                 0.f, -1.f, 0.f };
}

// Overwritten to use sharpen function.
void SharpenFilter::scanImage()
{
    sharpen();
}

// Applies a sharpen filter to the image. First convolves in the x-direction, storing
// calculated energy values in the m_tempPix vector. Then convolves in the y-direction,
// updating the actual pixel data with the newly calculated energy values.
void SharpenFilter::sharpen()
{
    BGRA* pix = m_canvas->data();
    int width = m_canvas->width();
    int height = m_canvas->height();

    m_tempPix.reserve(2 *width * height);

    int rowstart, rowend, rowcounter, colstart, colend, colcounter, index,
            currKerIndex, currPixIndex;

    float sumR, sumG, sumB;

    // No user selection, apply filter to the entire image.
    if (m_canvas->marqueeStart().x() == m_canvas->marqueeStop().x() &&
            m_canvas->marqueeStart().y() == m_canvas->marqueeStop().y()) {
        rowstart = 0;
        rowend = height;
        colstart = 0;
        colend = width;
    }
    // Check for edge case (for when stop < start);
    else {
        // Use min/max to find the proper starting/ending points for each edge of the marquee
        // selection.
        rowstart = max(min(m_canvas->marqueeStart().y(), m_canvas->marqueeStop().y()), 0);
        rowend = min(max(m_canvas->marqueeStop().y(), m_canvas->marqueeStart().y()), m_canvas->height());
        colstart = max(min(m_canvas->marqueeStart().x(), m_canvas->marqueeStop().x()), 0);
        colend = min(max(m_canvas->marqueeStop().x(), m_canvas->marqueeStart().x()), m_canvas->width());
    }

    // X-convolution
    for (rowcounter = rowstart; rowcounter < rowend; rowcounter++) {
        for (colcounter = colstart; colcounter < colend; colcounter++) {

            // Index of current pixel.
            index = get1DIndex(width, colcounter, rowcounter);
            sumR = 0.f;
            sumG = 0.f;
            sumB = 0.f;

            // Loop through 3x3 kernel
            // Row
            for (int y = -1; y <= 1; y++) {
                int row = glm::clamp(rowcounter + y, 0, height - 1);
                // Col
                for (int x = -1; x <= 1; x++) {
                    // Get index of kernel
                    currKerIndex = get1DIndex(3, x + 1, y + 1);

                    // Now calculate index of actual pixel data
                    int column = glm::clamp(colcounter + x, 0, width - 1);
                    currPixIndex = get1DIndex(width, column, row);

                    sumR += m_kernel[currKerIndex] * pix[currPixIndex].r;
                    sumG += m_kernel[currKerIndex] * pix[currPixIndex].g;
                    sumB += m_kernel[currKerIndex] * pix[currPixIndex].b;
                }
            }
            m_tempPix[index] = glm::vec3(sumR, sumG, sumB);
        }
    }

    // Overwrite pixel data with new calculated data.
    for (rowcounter = rowstart; rowcounter < rowend; rowcounter++) {
        for (colcounter = colstart; colcounter < colend; colcounter++) {
            // Index of current pixel.
            index = get1DIndex(width, colcounter, rowcounter);
            pix[index].r = glm::clamp(m_tempPix[index].x, 0.f, 255.f);
            pix[index].g = glm::clamp(m_tempPix[index].y, 0.f, 255.f);
            pix[index].b = glm::clamp(m_tempPix[index].z, 0.f, 255.f);
        }
    }
}
