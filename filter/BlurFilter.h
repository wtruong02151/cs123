#ifndef BLURFILTER_H
#define BLURFILTER_H

#include "Filter.h"
#include "Settings.h"

class BlurFilter
    : public Filter
{
public:
    BlurFilter(Canvas2D* canvas);

    virtual void scanImage();

private:
    // Function that applies the blur filter on the image.
    void blur();

    // Function that makes the kernel for a given sigma value.
    void makeKernel(int sigma);

    // Array that stores modifed pixel values.
    std::vector<glm::vec3> m_tempPix;

    // Array for the kernel values.
    std::vector<float> m_kernel;

    // Radius of kernel
    int m_radius;
};

#endif // BLURFILTER_H
