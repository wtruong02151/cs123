#ifndef SCALEFILTER_H
#define SCALEFILTER_H

#include "Filter.h"
#include "Settings.h"


class ScaleFilter
    : public Filter
{
public:
    ScaleFilter(Canvas2D *canvas);

    virtual void scanImage();

private:

    // Takes in a scale factor (x or y, doesn't matter).
    void scaleX(float scaleFactor);

    void scaleY(float scaleFactor);

    float backMapping(int index, float scale);

    std::vector<glm::vec3> m_tempPix;
};


#endif // SCALEFILTER_H
