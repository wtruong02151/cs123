#ifndef EDGEDETECTFILTER_H
#define EDGEDETECTFILTER_H

#include "Filter.h"
#include "Settings.h"

class EdgeDetectFilter
    : public Filter
{
public:
    EdgeDetectFilter(Canvas2D* canvas);

    virtual void scanImage();

private:
    // Function that applies edge detect on the image.
    void edgeDetect();

    // Array that stores modifed pixel values. Vector
    // of vec2's to store the Gx/GyTemp values.
    std::vector<glm::vec2> m_tempPix;
};

#endif // EDGEDETECTFILTER_H
