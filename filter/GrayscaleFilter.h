#ifndef GRAYSCALEFILTER_H
#define GRAYSCALEFILTER_H

#include "Filter.h"

class GrayscaleFilter
    : public Filter
{
public:
    GrayscaleFilter(Canvas2D* canvas);

private:
    // Virtual function to be overwritten.
    virtual void applyFilter(BGRA* imageData, int index);
};

#endif // GRAYSCALEFILTER_H
