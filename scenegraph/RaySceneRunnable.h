#ifndef RAYSCENERUNNABLE_H
#define RAYSCENERUNNABLE_H

#include <QThread>
#include <QThreadPool>
#include <QRunnable>
#include "Scene.h"
#include "RayScene.h"

class RaySceneRunnable : public QRunnable
{
public:
    RaySceneRunnable(RayScene *rayScene, Canvas2D *canvas, Camera *camera, int width, int height, int startingRow, int endingRow);

    // Virtual function to be overwritten
    virtual void run();

private:

    // Member variables needed to be used in run().
    RayScene *m_rayScene;
    Canvas2D *m_canvas;
    Camera *m_camera;
    int m_width, m_height, m_startingRow, m_endingRow;

};

#endif // RAYSCENERUNNABLE_H
