#ifndef SCENE_H
#define SCENE_H

#include <CS123Common.h>
#include "CS123SceneData.h"

class Camera;
class CS123ISceneParser;


/**
 * Struct used to store information relative
 * objects and its transformations within the scene graph.
 */
struct transformedObject {
    // Composite transormation matrix.
    glm::mat4 transMat;
    // Primitive shape to be drawn.
    CS123ScenePrimitive primitive;
    // Store texture since this ISH gets deleted from scene
    CS123SceneFileMap *textureMap;


    transformedObject(CS123ScenePrimitive prim, glm::mat4 trans, CS123SceneFileMap *texture) {
//        transformedObject(CS123ScenePrimitive prim, glm::mat4 trans) {
        transMat = trans;
        primitive = prim;
        textureMap = texture;
    }

};


/**
 * @class Scene
 *
 * @brief This is the base class for all scenes. Modify this class if you want to provide
 * common functionality to all your scenes.
 */
class Scene
{
public:
    Scene();
    virtual ~Scene();

    static void parse(Scene *sceneToFill, CS123ISceneParser *parser);

    // Returns vector of scene primitives and their transformations.
    std::vector<transformedObject> getTransObjs();

    // Returns vector of scene lights.
    std::vector<CS123SceneLightData> getLights();

    // Returns scene global data.
    CS123SceneGlobalData getGlobal();
protected:

    CS123SceneGlobalData m_globalData;

    // List of transformed objects; filled as we traverse the scene graph.
    std::vector<transformedObject> m_sceneObjects;

    // List of lights.
    std::vector<CS123SceneLightData> m_lights;

    // Function used to traverse the tree. Takes in a particular node
    // to start the traversal (usually the root); iterative traversal using
    // a queue.
    static void recurTraversal(Scene *sceneToFill, CS123SceneNode &node, glm::mat4x4 prevTrans);

    // Adds a primitive to the scene.
    virtual void addPrimitive(const CS123ScenePrimitive &scenePrimitive, const glm::mat4x4 &matrix);

    // Adds a light to the scene.
    virtual void addLight(const CS123SceneLightData &sceneLight);

    // Sets the global data for the scene.
    virtual void setGlobal(const CS123SceneGlobalData &global);

};

#endif // SCENE_H
