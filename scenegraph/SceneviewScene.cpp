#include "SceneviewScene.h"
#include "GL/glew.h"
#include <QGLWidget>
#include "Camera.h"


SceneviewScene::SceneviewScene()
{
    // TODO: [SCENEVIEW] Set up anything you need for your Sceneview scene here...


}

SceneviewScene::~SceneviewScene()
{
    // TODO: [SCENEVIEW] Don't leak memory!

}


void SceneviewScene::init()
{
    OpenGLScene::init();
    // Initialize the vertex array object.

    m_cube.reset(new Cube(25, 25, m_shader, m_normalRenderer));
    m_cone.reset(new cone(25, 25, m_shader, m_normalRenderer));
    m_cylinder.reset(new cylinder(25, 25, m_shader, m_normalRenderer));
    m_sphere.reset(new sphere(25, 25, m_shader, m_normalRenderer));
}

void SceneviewScene::setLights(const glm::mat4 viewMatrix)
{
    // TODO: [SCENEVIEW] Fill this in...
    //
    // Use function(s) inherited from OpenGLScene to set up the lighting for your scene.
    // The lighting information will most likely be stored in CS123SceneLightData structures.
    //

    glUniformMatrix4fv(m_uniformLocs["v"], 1, GL_FALSE,
            glm::value_ptr(viewMatrix));

    for (int i = 0; i < m_lights.size(); i++) {
        setLight(m_lights[i]);
    }

}

void SceneviewScene::renderGeometry()
{
    // TODO: [SCENEVIEW] Fill this in...
    //
    // This is where you should render the geometry of the scene. Use what you
    // know about OpenGL and leverage your Shapes classes to get the job done.
    //

    // Loop through m_sceneObjects and render them each based on their primitive types.
    for (transformedObject t : m_sceneObjects) {
        applyMaterial(t.primitive.material);

        // Apply transformation.
        glUniformMatrix4fv(glGetUniformLocation(m_shader, "m"), 1, GL_FALSE,
                    glm::value_ptr(t.transMat));

        switch(t.primitive.type) {
        case PRIMITIVE_CUBE:
            m_cube->draw();
            break;
        case PRIMITIVE_CONE:
            m_cone->draw();
            break;
        case PRIMITIVE_CYLINDER:
            m_cylinder->draw();
            break;
        case PRIMITIVE_SPHERE:
            m_sphere->draw();
            break;
        }
    }
}


void SceneviewScene::setSelection(int x, int y)
{
    // TODO: [MODELER LAB] Fill this in...
    //
    // Using m_selectionRecorder, set m_selectionIndex to the index in your
    // flattened parse tree of the object under the mouse pointer.  The
    // selection recorder will pick the object under the mouse pointer with
    // some help from you, all you have to do is:

    // 1) Set this to the number of objects you will be drawing.
    int numObjects = 0;

    // 2) Start the selection process
    m_selectionRecorder.enterSelectionMode(x, y, numObjects);

    // 3) Draw your objects, calling m_selectionRecorder.setObjectIndex() before drawing each one.

    // 4) Find out which object you selected, if any (-1 means no selection).
    m_selectionIndex = m_selectionRecorder.exitSelectionMode();
}

