#include "Canvas2D.h"
#include "RayScene.h"
#include "Settings.h"
#include "CS123SceneData.h"
#include <QCoreApplication>


RayScene::RayScene(std::vector<transformedObject> sceneObjs, std::vector<CS123SceneLightData> lights, CS123SceneGlobalData global)
{
    m_sceneObjects = sceneObjs;
    m_lights = lights;
    m_globalData = global;

    // Loop through all of the scene objects and load the image textures at the start.
    for (transformedObject shape : m_sceneObjects) {
        // If the textureMap is used, then store it in our hashmap.
        if (shape.textureMap->isUsed) {
            QString filename = QString(shape.textureMap->filename.c_str());
            QImage image(filename);
            image.load(filename, 0);
            m_textureHashMap.insert(filename, image);
        }
    }
}

RayScene::~RayScene()
{

}

void RayScene::filterImage(Canvas2D *canvas, Camera *camera, int width, int height, int startingRow, int endingRow)
{
    // @TODO: raytrace the scene based on settings
    //        YOU MUST FILL THIS IN FOR INTERSECT/RAY

    // If you want the interface to stay responsive, make sure to call
    // QCoreApplication::processEvents() periodically during the rendering.

    // Grab proper matrices to transform pFilm to pWorld.
    // (pworld = mFilmToWorld * pfilm).
    glm::mat4x4 m4m3Inverse = glm::inverse(camera->getViewMatrix());

    // Dynamic cast to grab scale matrix.
    glm::mat4x4 m2Inverse = glm::inverse(dynamic_cast<CamtransCamera*>(camera)->getScaleMatrix());

    // Calculate mFilmToWorld by multiple (M4^-1)(M3^-1)(M2^-1).
    glm::mat4x4 mFilmToWorld = m4m3Inverse * m2Inverse;

    // Calculate pEye.
    glm::vec4 pEye = mFilmToWorld * glm::vec4(0.0, 0.0, 0.0, 1.0);

    // Loop through screen space (with given width and height of screen)
    // and generate a ray for each pixel.
    for (int row = startingRow; row < endingRow; row++) {
        canvas->update();
        QCoreApplication::processEvents();
        for (int col = 0; col < width; col++) {
            // Calculate pFilm when iterating through width and height of image.
            glm::vec4 pFilm = glm::vec4(((2.0 * col) / width) - 1.0, 1.0 - ((2.0 * row) / height), -1.0, 1.0);

            // Convert film space to world space.
            glm::vec4 pWorld = mFilmToWorld * pFilm;
            // Calculate direction for the ray we're going to create.

            glm::vec4 direction = glm::normalize(pWorld - pEye);
            direction.w = 0.0;

            // Create the ray and add it to our vector of rays.
            CS123Ray ray(pEye, direction);

            // Shoot the ray into the scene and grab the value of t in which
            // the ray intserects.
            glm::vec3 color = shootRay(ray, 0, false, row, col, width, height);

            int index = (width * row) + col;

            canvas->data()[index].r = color.r;
            canvas->data()[index].g = color.g;
            canvas->data()[index].b = color.b;
        }
    }
    std::cout<<"done rendering"<<endl;
}

// Function that, for a given ray, loop through all
// shapes (primitives) in scene and check if they intersect.
glm::vec3 RayScene::shootRay(CS123Ray ray, int numReflections, bool shadowRecur, int row, int col, int width, int height)
{
    // Keep track of minimum t value, closest shape, and that shape's transofmration matrix
    // through iteration.
    float currT;
    float minT = INFINITY;
    CS123ScenePrimitive closestShape;
    glm::mat4x4 closestTransMat;
    CS123SceneFileMap *closestTexture;
    // Point of intersection for closest object. Used to calculate surface normal.
    glm::vec4 worldIntersection, objIntersection;
    // Int flag for if the ray intersected a partiular plane (for cube, cylinder or cone).
    int face = 0;
    for (transformedObject shape : m_sceneObjects) { 
        // Transform ray point and direction into object space to check for intersection.
        glm::vec4 objectRayPoint = glm::inverse(shape.transMat) * ray.point;
        glm::vec4 objectRayDir =  glm::inverse(shape.transMat) * ray.direction;
        // Switch to check what kind of shape we're dealing with
        // since each shape has its own implicit equation to solve
        // for t.
        switch(shape.primitive.type) {
        case PRIMITIVE_CUBE:
        {
            glm::vec2 temp = implicitCube(objectRayPoint, objectRayDir);
            currT = temp.x;
            // Newest closest object, update minT and closestShape.
            if (minT > currT) {
                minT = currT;
                closestShape = shape.primitive;
                closestTransMat = shape.transMat;
                closestTexture = shape.textureMap;
                face = temp.y;
                // TESTING
                objIntersection = glm::vec4(objectRayPoint.x + objectRayDir.x * minT,
                                            objectRayPoint.y + objectRayDir.y * minT,
                                            objectRayPoint.z + objectRayDir.z * minT,
                                            1.0);
                // Calculate point of intersection in world space (used for lighting equation).
                worldIntersection = glm::vec4(ray.point.x + ray.direction.x * minT,
                                              ray.point.y + ray.direction.y * minT,
                                              ray.point.z + ray.direction.z * minT,
                                              1.0);
            }
            break;
        }
        case PRIMITIVE_CONE:
        {
            glm::vec2 temp = implicitCone(objectRayPoint, objectRayDir);
            currT = temp.x;
            // Newest closest object, update minT and closestShape.
            if (minT > currT) {
                minT = currT;
                closestShape = shape.primitive;
                closestTransMat = shape.transMat;
                closestTexture = shape.textureMap;
                face = temp.y;
                // TESTING
                objIntersection = glm::vec4(objectRayPoint.x + objectRayDir.x * minT,
                                            objectRayPoint.y + objectRayDir.y * minT,
                                            objectRayPoint.z + objectRayDir.z * minT,
                                            1.0);
                // Calculate point of intersection in world space (used for lighting equation and calculating normal).
                worldIntersection = glm::vec4(ray.point.x + ray.direction.x * minT,
                                              ray.point.y + ray.direction.y * minT,
                                              ray.point.z + ray.direction.z * minT,
                                              1.0);
            }
            break;
        }
        case PRIMITIVE_CYLINDER:
        {
            glm::vec2 temp = implicitCylinder(objectRayPoint, objectRayDir);
            currT = temp.x;
            // Newest closest object, update minT and closestShape.
            if (minT > currT) {
                minT = currT;
                closestShape = shape.primitive;
                closestTransMat = shape.transMat;
                closestTexture = shape.textureMap;
                face = temp.y;
                // TESTING
                objIntersection = glm::vec4(objectRayPoint.x + objectRayDir.x * minT,
                                            objectRayPoint.y + objectRayDir.y * minT,
                                            objectRayPoint.z + objectRayDir.z * minT,
                                            1.0);
                // Calculate point of intersection in world space (used for lighting equation and calculating normal).
                worldIntersection = glm::vec4(ray.point.x + ray.direction.x * minT,
                                              ray.point.y + ray.direction.y * minT,
                                              ray.point.z + ray.direction.z * minT,
                                              1.0);
            }
            break;
        }
        case PRIMITIVE_SPHERE:
        {
            currT = implicitSphere(objectRayPoint, objectRayDir);
            // Newest closest object, update minT and closestShape.
            if (minT > currT) {
                minT = currT;
                closestShape = shape.primitive;
                closestTransMat = shape.transMat;
                closestTexture = shape.textureMap;
                // TESTING
                objIntersection = glm::vec4(objectRayPoint.x + objectRayDir.x * minT,
                                            objectRayPoint.y + objectRayDir.y * minT,
                                            objectRayPoint.z + objectRayDir.z * minT,
                                            1.0);
                // Calculate point of intersection in world space (used for lighting equation and calculating normal).
                worldIntersection = glm::vec4(ray.point.x + ray.direction.x * minT,
                                              ray.point.y + ray.direction.y * minT,
                                              ray.point.z + ray.direction.z * minT,
                                              1.0);
            }
            break;
        }
        }
    }

    // Forward declarations
    glm::vec2 uv = glm::vec2(0.f, 0.f);
    glm::vec2 st = glm::vec2(0.f, 0.f);
    glm::vec3 finalReflectedColor = glm::vec3(0.f, 0.f, 0.f);
    glm::vec3 currentColor = glm::vec3(0.f, 0.f, 0.f);
    glm::vec3 currReflColor = glm::vec3(0.f, 0.f, 0.f);
    QColor textData = QColor(0, 0, 0);
    float blend = 0.f;
    // If the ray actually intersected a shape, find calculate the final intensities
    // and return them. Can change number of total reflections we allow in our calculations
    // (currently set to 3 bounces
    if (minT != INFINITY) {
        // If texturemapping setting is set
        if (settings.useTextureMapping) {
            // Check if texture is actually used
            if (closestTexture->isUsed) {
                switch(closestShape.type) {
                case PRIMITIVE_CUBE:
                    uv = getUVCube(objIntersection, face);
                    break;
                case PRIMITIVE_CONE:
                    uv = getUVConeCylinder(objIntersection, face);
                    break;
                case PRIMITIVE_CYLINDER:
                    uv = getUVConeCylinder(objIntersection, face);
                    break;
                case PRIMITIVE_SPHERE:
                    uv = getUVSphere(objIntersection);
                    break;
                }
                float j = closestTexture->repeatU;
                float k = closestTexture->repeatV;
                blend = closestShape.material.blend;
                QImage image = m_textureHashMap.value(closestTexture->filename.c_str());
                st = getTiledCoordinates(uv, j, k, image.width(), image.height());
                textData = QColor(image.pixel(st.x, st.y));
            }
        }

        // Here, check if we've intersected with an object for the obstruction of the light source (return distance as well).
        if (shadowRecur) {
            // Calculate distance between original object and object obstructing light source.
            float distanceToObstruction = glm::length(glm::vec3(worldIntersection - ray.point));
            // We set the x-component to 1 as a flag to show that we've intersected an object.
            return glm::vec3(1, distanceToObstruction, 0);
        }
        // Normal of intersection
        glm::vec3 normalOfIncidence = getNormal(objIntersection, closestShape.type, closestTransMat, face);
        // Calculate color of current shape at intersection (not multiplied by 255.f).
        currentColor = finalIntensity(closestShape, worldIntersection,
                                      normalOfIncidence, ray.direction, textData, blend);

        ////////////////////////////////////////////////////////////////////////////
        // ***** HEY TA!!!! ADJUST THE NUMBER OF REFLECTION BOUNCES HERE!!! ***** //
        ////////////////////////////////////////////////////////////////////////////

        // Check if we can reflect, if we can, the recursively call shootRay.
        if (settings.useReflection && numReflections < 8) {
            // Calculate reflection vector based on normal and ray direction.
            glm::vec3 reflectionVector = glm::normalize(glm::reflect(glm::vec3(ray.direction), normalOfIncidence));
            // Calculate offsetIntersection
            glm::vec4 offsetIntersection = worldIntersection + (0.1f * glm::vec4(reflectionVector, 0.f));
            // Instantiate new ray to be shot in the direction of the reflection vector.
            CS123Ray recurRay(offsetIntersection, glm::vec4(reflectionVector, 0.f));
            // Shoot reflected ray
            numReflections += 1;
            currReflColor = shootRay(recurRay, numReflections, false, row, col, width, height);
        }
        finalReflectedColor = glm::vec3(currReflColor.x * closestShape.material.cReflective.r * m_globalData.ks,
                                         currReflColor.y * closestShape.material.cReflective.g * m_globalData.ks,
                                         currReflColor.z * closestShape.material.cReflective.b * m_globalData.ks);
    }
    // Check if we didn't intersect an object, and we're on the shadow recur
    else if (minT == INFINITY && shadowRecur) {
        return glm::vec3(0, 0, 0);
    }

    // Final calculation for summation of reflected lights.
    currentColor.x = glm::clamp(currentColor.x * 255.f + finalReflectedColor.x, 0.f, 255.f);
    currentColor.y = glm::clamp(currentColor.y * 255.f + finalReflectedColor.y, 0.f, 255.f);
    currentColor.z = glm::clamp(currentColor.z * 255.f + finalReflectedColor.z, 0.f, 255.f);

    // Return current color calculation if no intersection (black if we hit nothing,
    // otherwise return the final calculated color taking into account all reflected colors.
    return currentColor;
}

glm::vec3 RayScene::finalIntensity(CS123ScenePrimitive prim, glm::vec4 pWorldInter,
                                   glm::vec3 surfaceNormal, glm::vec4 rayDirection,
                                   QColor textureData, float blend)
{
    // Ambient color + blend * textureData
    float finalRed = (m_globalData.ka * prim.material.cAmbient.r);
    float finalGreen = (m_globalData.ka * prim.material.cAmbient.g);
    float finalBlue = (m_globalData.ka * prim.material.cAmbient.b);

    glm::vec3 diffuseColor = glm::vec3(blend * textureData.redF() + ((1 - blend) * prim.material.cDiffuse.r),
                                       blend * textureData.greenF() + ((1 - blend) * prim.material.cDiffuse.g),
                                       blend * textureData.blueF() + ((1 - blend) * prim.material.cDiffuse.b));

    // Loop through all lights to finalize calculations.
    for (CS123SceneLightData light : m_lights) {
        // Light attenuation set initially to 1.f (since directional lighting
        // does not vary in light attenuation.
        float fatt = 1.f;

        // Light attenuation function for the given light.
        // (applicable to point lighting).
        glm::vec3 lightFunc = light.function;

        // Light direction is calculating by taking the difference between
        // the light's position and the point of intersection for the current
        // primitive.
        glm::vec4 lightDir = glm::normalize(light.pos - pWorldInter);

        // Length of this light direction.
        float lightDirLength = glm::length(light.pos - pWorldInter);

        // Used in calculation of lighting equation.
        float normDotLight, reflDotV;

        glm::vec3 reflectionVector, viewpointVector;

        switch(light.type) {
        case LIGHT_POINT:
        {
            // Calculations for attenuation
            if (settings.usePointLights) {
                float attenuation = 1 / (lightFunc.x + (lightFunc.y * lightDirLength) + (lightFunc.z * powf(lightDirLength, 2.f)));
                fatt = std::min(1.f, attenuation);
                if (settings.useShadows) {
                    // Calculate offsetIntersection
                    glm::vec4 offsetIntersection = pWorldInter + (0.1f * lightDir);
                    // Check if this light is covered by some other object by shooting a ray.
                    CS123Ray rayToLight(offsetIntersection, lightDir);
                    // Recursively shoot ray, then check what's returned to see if we should use this light or not.
                    // Doesn't matter what we pass in for row, col, width and height here.
                    glm::vec3 checkIfObstructed = shootRay(rayToLight, INFINITY, true, 0, 0, 0, 0);
                    // If the light is blocked by some object, ignore this light's contribution
                    if (checkIfObstructed.x == 1) {
                        // Grab distance to obstruction here.
                        float distanceToObstruction = checkIfObstructed.y;
                        // We've obstructed the light, continue.
                        if (distanceToObstruction < lightDirLength) {
                            continue;
                        }
                    }
                }
            }
            else {
                continue;
            }
            break;
        }
        case LIGHT_DIRECTIONAL:
        {
            if (settings.useDirectionalLights) {
                lightDir = glm::normalize(-light.dir);
                if (settings.useShadows) {
                    // Calculate offsetIntersection
                    glm::vec4 offsetIntersection = pWorldInter + (0.1f * lightDir);
                    // Check if this light is covered by some other object by shooting a ray.
                    CS123Ray rayToLight(offsetIntersection, lightDir);
                    // Recursively shoot ray, then check what's returned to see if we should use this light or not.
                    glm::vec3 checkIfObstructed = shootRay(rayToLight, INFINITY, true, 0, 0, 0, 0);
                    // If the light is blocked by some object, ignore this light's contribution
                    if (checkIfObstructed.x) {
                        continue;
                    }
                }
            }
            else {
                continue;
            }
            break;
        }
        }
        normDotLight = max(0.f, glm::dot(glm::normalize(surfaceNormal), glm::vec3(lightDir)));
        // Calculations for specular lighting and relfection vector.
        reflectionVector = glm::normalize(glm::reflect(glm::vec3(lightDir), surfaceNormal));
        viewpointVector = glm::normalize(glm::vec3(rayDirection));
        reflDotV = powf(max(0.f, glm::dot(reflectionVector, viewpointVector)), prim.material.shininess);

        // Final summations for light intensities.
        finalRed += fatt * light.color.r * ((m_globalData.kd * diffuseColor.x * normDotLight) + (m_globalData.ks * prim.material.cSpecular.r * reflDotV));
        finalGreen += fatt * light.color.g * ((m_globalData.kd * diffuseColor.y * normDotLight) + (m_globalData.ks * prim.material.cSpecular.g * reflDotV));
        finalBlue += fatt * light.color.b * ((m_globalData.kd * diffuseColor.z * normDotLight) + (m_globalData.ks * prim.material.cSpecular.b * reflDotV));
    }
    return glm::vec3(finalRed, finalGreen, finalBlue);
}

glm::vec2 RayScene::implicitCube(glm::vec4 point, glm::vec4 direction)
{
    // Forward declarations.
    float t = INFINITY;
    float tTemp, x, y, z;
    glm::vec4 normal, plane;

    // Flag used to determine normal for face of intersection (issue with floating point precision
    // when checking if implicit equation hits exactly 0.5 on a particular plane).
    int faceFlag = 0; // 1 = front face, 2 = right face, 3 = back face, 4 = left face, 5 = top face, 6 = bottom face.

    // Front face (xy plane; normal is positive z and plane is 0.5 z)
    normal = glm::vec4(0.0, 0.0, 1.0, 0.0);
    plane = glm::vec4(0.0, 0.0, 0.5, 0.0);
    tTemp = intersectPlane(normal, plane, point, direction);

    if (tTemp > 0) {
        // Check face bounds
        x = point.x + direction.x * tTemp;
        y = point.y + direction.y * tTemp;
        if (x <= 0.5 && x >= -0.5 && y <= 0.5 && y >= -0.5) {
            // this intersection is the closest, so update face flag
            if (tTemp < t) {
                faceFlag = 1;
            }
            t = min(t, tTemp);
        }
    }

    // right face (-zy plane; normal is positive x and plane is 0.5 x)
    normal = glm::vec4(1.0, 0.0, 0.0, 0.0);
    plane = glm::vec4(0.5, 0.0, 0.0, 0.0);
    tTemp = intersectPlane(normal, plane, point, direction);

    if (tTemp > 0) {
        // Check face bounds
        z = point.z + direction.z * tTemp;
        y = point.y + direction.y * tTemp;
        if (z <= 0.5 && z >= -0.5 && y <= 0.5 && y >= -0.5) {
            // this intersection is the closest, so update face flag
            if (tTemp < t) {
                faceFlag = 2;
            }
            t = min(t, tTemp);
        }
    }

    // back face (xy plane; normal is negative z and plane is -0.5 z)
    normal = glm::vec4(0.0, 0.0, -1.0, 0.0);
    plane = glm::vec4(0.0, 0.0, -0.5, 0.0);
    tTemp = intersectPlane(normal, plane, point, direction);

    if (tTemp > 0) {
        // Check face bounds
        x = point.x + direction.x * tTemp;
        y = point.y + direction.y * tTemp;
        if (x <= 0.5 && x >= -0.5 && y <= 0.5 && y >= -0.5) {
            // this intersection is the closest, so update face flag
            if (tTemp < t) {
                faceFlag = 3;
            }
            t = min(t, tTemp);
        }
    }

    // left face (zy plane; normal is -x and plane is -0.5 x)
    normal = glm::vec4(-1.0, 0.0, 0.0, 0.0);
    plane = glm::vec4(-0.5, 0.0, 0.0, 0.0);
    tTemp = intersectPlane(normal, plane, point, direction);

    if (tTemp > 0) {
        // Check face bounds
        z = point.z + direction.z * tTemp;
        y = point.y + direction.y * tTemp;
        if (z <= 0.5 && z >= -0.5 && y <= 0.5 && y >= -0.5) {
            // this intersection is the closest, so update face flag
            if (tTemp < t) {
                faceFlag = 4;
            }
            t = min(t, tTemp);
        }
    }

    // top face (-zx plane; normal is y and plane is 0.5 y);
    normal = glm::vec4(0.0, 1.0, 0.0, 0.0);
    plane = glm::vec4(0.0, 0.5, 0.0, 0.0);
    tTemp = intersectPlane(normal, plane, point, direction);

    if (tTemp > 0) {
        // Check face bounds
        z = point.z + direction.z * tTemp;
        x = point.x + direction.x * tTemp;
        if (z <= 0.5 && z >= -0.5 && x <= 0.5 && x >= -0.5) {
            // this intersection is the closest, so update face flag
            if (tTemp < t) {
                faceFlag = 5;
            }
            t = min(t, tTemp);
        }
    }

    // bottom face (zx plane; normal is -y and plane is -0.5 y);
    normal = glm::vec4(0.0, -1.0, 0.0, 0.0);
    plane = glm::vec4(0.0, -0.5, 0.0, 0.0);
    tTemp = intersectPlane(normal, plane, point, direction);

    if (tTemp > 0) {
        // Check face bounds
        z = point.z + direction.z * tTemp;
        x = point.x + direction.x * tTemp;
        if (z <= 0.5 && z >= -0.5 && x <= 0.5 && x >= -0.5) {
            // this intersection is the closest, so update face flag
            if (tTemp < t) {
                faceFlag = 6;
            }
            t = min(t, tTemp);
        }
    }
    return glm::vec2(t, faceFlag);
}

glm::vec2 RayScene::implicitCone(glm::vec4 point, glm::vec4 direction)
{
    float A = (pow(direction.x, 2.0) + pow(direction.z, 2.0) - ((0.25) * pow(direction.y, 2.0)));
    float B = (2.0 * point.x * direction.x) + (2.0 * point.z * direction.z) - ((0.5) * point.y * direction.y) + ((0.25) * direction.y);
    float C = pow(point.x, 2.0) + pow(point.z, 2.0) - ((0.25) * pow(point.y, 2.0)) + ((0.25) * point.y) - (0.0625);

    // Set t to infinity.
    float t = INFINITY;
    float t1, t2, t3;

    // If closest point of intersection is the cap, then set the flag to -1.
    int capFlag = 0;

    // Check discriminant; if discriminant is greater than 0,
    // then we know that the ray intersects the body of the cone.
    // So grab the minimum of the intersection (first point of intersection).
    float discriminant = pow(B, 2.0) - (4.0 * A * C);
    if (discriminant >= 0.0) {
        t1 = ((-B) + sqrt(discriminant)) / (2.0 * A);
        t2 = ((-B) - sqrt(discriminant)) / (2.0 * A);

        float y1 = point.y + direction.y * t1;;
        float y2 = point.y + direction.y * t2;;

        // Check if t values are positive and that y is within the
        // bounds of the cone.
        if (t1 < 0.0 || y1 > 0.5 || y1 < -0.5) {t1 = INFINITY;}
        if (t2 < 0.0 || y2 > 0.5 || y2 < -0.5) {t2 = INFINITY;}

        t = min(t1, t2);
    }

    // Now check if ray intersect with cap of the cone.
    glm::vec4 normal = glm::vec4(0.0, -1.0, 0.0, 0.0);
    glm::vec4 plane = glm::vec4(0.0, -0.5, 0.0, 0.0);
    t3 = intersectPlane(normal, plane, point, direction);
    // If tTemp is positive, then we have a valid intersection with the plane.
    if (t3 > 0) {
        // Grab actual point of intersection on xz-plane.
        float x = point.x + direction.x * t3;
        float z = point.z + direction.z * t3;
        // Check if intersection is within the radius of the cap, not the entire plane.
        // If it is, then check set t equal to the first point of intersection.
        if (x*x + z*z <= 0.25) {
            if (t3 < t) {
                capFlag = 6;
            }
            t = min(t, t3);
        }
    }

    return glm::vec2(t, capFlag);
}

glm::vec2 RayScene::implicitCylinder(glm::vec4 point, glm::vec4 direction)
{
    float A = pow(direction.x, 2.0) + pow(direction.z, 2.0);
    float B = (2.0 * point.x * direction.x) + (2.0 * point.z * direction.z);
    float C = pow(point.x, 2.0) + pow(point.z, 2.0) - 0.25;

    // Set t to infinity.
    float t = INFINITY;
    float t1, t2, t3;

    // If closest point of intersection is the cap, then set the flag to -1.
    int capFlag = 0;

    // Check discriminant; if discriminant is greater than 0,
    // then we know that the ray intersects the body of the cylinder.
    // So grab the minimum of the intersection (first point of intersection).
    float discriminant = pow(B, 2.0) - (4.0 * A * C);
    if (discriminant >= 0.0) {
        t1 = ((-B) + sqrt(discriminant)) / (2.0 * A);
        t2 = ((-B) - sqrt(discriminant)) / (2.0 * A);

        float y1 = point.y + direction.y * t1;;
        float y2 = point.y + direction.y * t2;;

        // Check if t values are positive and that y is within the
        // bounds of the cone.
        if (t1 < 0.0 || y1 > 0.5 || y1 < -0.5) {t1 = INFINITY;}
        if (t2 < 0.0 || y2 > 0.5 || y2 < -0.5) {t2 = INFINITY;}

        t = min(t1, t2);
    }

    // Now check if ray intersect with bottom cap of the cylinder.
    glm::vec4 normal = glm::vec4(0.0, -1.0, 0.0, 0.0);
    glm::vec4 plane = glm::vec4(0.0, -0.5, 0.0, 0.0);
    t3 = intersectPlane(normal, plane, point, direction);
    // If tTemp is positive, then we have a valid intersection with the plane.
    if (t3 > 0) {
        // Grab actual point of intersection on xz-plane.
        float x = point.x + direction.x * t3;
        float z = point.z + direction.z * t3;
        // Check if intersection is within the radius of the cap, not the entire plane.
        // If it is, then check set t equal to the first point of intersection.
        if (x*x + z*z <= 0.25) {
            if (t3 < t) {
                capFlag = 6;
            }
            t = min(t, t3);
        }
    }

    // Now check if ray intersects with top cap of the cylinder.
    normal = glm::vec4(0.0, 1.0, 0.0, 0.0);
    plane = glm::vec4(0.0, 0.5, 0.0, 0.0);
    t3 = intersectPlane(normal, plane, point, direction);
    // If tTemp is positive, then we have a valid intersection with the plane.
    if (t3 > 0) {
        // Grab actual point of intersection on xz-plane.
        float x = point.x + direction.x * t3;
        float z = point.z + direction.z * t3;
        // Check if intersection is within the radius of the cap, not the entire plane.
        // If it is, then check set t equal to the first point of intersection.
        if (x*x + z*z <= 0.25) {
            if (t3 < t) {
                capFlag = 5;
            }
            t = min(t, t3);
        }
    }

    return glm::vec2(t, capFlag);
}

float RayScene::implicitSphere(glm::vec4 point, glm::vec4 direction)
{
    float A = pow(direction.x, 2.0) + pow(direction.y, 2.0) + pow(direction.z, 2.0);
    float B = (2.0 * point.x * direction.x) + (2.0 * point.y * direction.y) + (2.0 * point.z * direction.z);
    float C = pow(point.x, 2.0) + pow(point.y, 2.0) + pow(point.z, 2.0) - 0.25;
    // Set t to infinity.
    float t = INFINITY;
    float t1, t2;

    float discriminant = pow(B, 2.0) - (4.0 * A * C);
    // Check discriminant; if discriminant is greater than 0,
    // then we know that the ray intersects the body of the cone.
    // So grab the minimum of the intersection (first point of intersection).
    if (discriminant >= 0.0) {
        t1 = ((-B) + sqrt(discriminant)) / (2.0 * A);
        t2 = ((-B) - sqrt(discriminant)) / (2.0 * A);

        // Check if t values are positive and that y is within the
        // radius of the sphere
        if (t1 < 0.0) {t1 = INFINITY;}
        if (t2 < 0.0) {t2 = INFINITY;}

        t = min(t1, t2);
    }
    return t;
}

float RayScene::intersectPlane(glm::vec4 normal, glm::vec4 plane, glm::vec4 point, glm::vec4 direction)
{
    return (((normal.x * plane.x) + (normal.y * plane.y) + (normal.z * plane.z)) - ((normal.x * point.x) + (normal.y * point.y) + (normal.z * point.z)))
            / ((normal.x * direction.x) + (normal.y * direction.y) + (normal.z * direction.z));
}

glm::vec3 RayScene::getNormal(glm::vec4 intersectionPoint, PrimitiveType shape, glm::mat4x4 transMat, int face)
{
    // Normal at given intersection (to be returned).
    glm::vec3 normalAtIntersection = glm::vec3(0.0, 0.0, 0.0);
    // Upper 3x3 transformation matrix (excludes translation matrix).
    glm::mat3x3 upperTransMat = glm::transpose(glm::inverse(glm::mat3x3(transMat)));
    switch(shape) {
    case PRIMITIVE_CUBE:
    {
        // Intersect on front plane, so return x = 1.
        if (face == 1) {
            normalAtIntersection = glm::vec3(0.f, 0.f, 1.f);
        }
        // Intersect on right plane, so return x = 1.
        else if (face == 2) {
            normalAtIntersection = glm::vec3(1.f, 0.f, 0.f);
        }
        // Intersected on back plane, so return x = -1.
        else if (face == 3) {
            normalAtIntersection = glm::vec3(0.f, 0.f, -1.f);
        }
        // Intersected on left plane, so return x = -1.
        else if (face == 4) {
            normalAtIntersection = glm::vec3(-1.f, 0.f, 0.f);
        }
        // Intersect on top plane, so return y = 1.
        else if (face == 5) {
            normalAtIntersection = glm::vec3(0.f, 1.f, 0.f);
        }
        // Intersected on bottom plane, so return y = -1.
        else if (face == 6) {
            normalAtIntersection = glm::vec3(0.f, -1.f, 0.f);
        }
        break;
    }
    case PRIMITIVE_CONE:
    {
        // Check if normal is from the bottom of the cone.
        if (face == 6) {
            normalAtIntersection = glm::vec3(0.f, -1.f, 0.f);
        }
        else {
            glm::vec3 temp = (2.f / sqrtf(5.f)) * glm::normalize(glm::vec3(intersectionPoint.x, 0, intersectionPoint.z));
            normalAtIntersection = glm::vec3(temp.x, (1.f / sqrtf(5.f)), temp.z);
        }
        break;
    }
    case PRIMITIVE_CYLINDER:
    {
        if (face == 6) {
            normalAtIntersection = glm::vec3(0.f, -1.f, 0.f);
        } else if (face == 5) {
            normalAtIntersection = glm::vec3(0.f, 1.f, 0.f);
        } else {
            normalAtIntersection = (2.f / sqrtf(5.f)) * glm::normalize(glm::vec3(intersectionPoint.x, 0, intersectionPoint.z));
        }
        break;
    }
    case PRIMITIVE_SPHERE:
    {
        normalAtIntersection = glm::normalize(glm::vec3(intersectionPoint));
        break;
    }
    }
    return glm::normalize(upperTransMat * normalAtIntersection);;
}

glm::vec2 RayScene::getUVCube(glm::vec4 objSpaceIntersection, int plane)
{
    return getUVPlane(objSpaceIntersection, plane);
}

glm::vec2 RayScene::getUVConeCylinder(glm::vec4 objSpaceIntersection, int plane)
{
    // If plane is not equal to 0, this means we've intersected one of the
    // caps of the cylinder and must texture map the cap.
    if (plane != 0) {
        return getUVPlane(objSpaceIntersection, plane);
    }
    float u;
    float v = -objSpaceIntersection.y + 0.5f;
    float theta = atan2(objSpaceIntersection.z, objSpaceIntersection.x);
    if (theta < 0) {
        u = -theta / (2 * M_PI);
    }
    else {
        u = 1.f - (theta / (2 * M_PI));
    }
    return glm::vec2(u, v);
}

glm::vec2 RayScene::getUVSphere(glm::vec4 objSpaceIntersection)
{
    float u, v;
    // Case when we're at the poles of the sphere
    if (objSpaceIntersection.y == 0.5f) {
        u = 0.5f;
        v = 0.0f;
        return glm::vec2(u, v);
    }
    else if (objSpaceIntersection.y == -0.5f) {
        u = 0.5f;
        v = 1.0f;
        return glm::vec2(u, v);
    }
    // Not at the poles, so do proper calculations.
    float theta = atan2(objSpaceIntersection.z, objSpaceIntersection.x);
    if (theta < 0) {
        u = -theta / (2 * M_PI);
    }
    else {
        u = 1.f - (theta / (2 * M_PI));
    }
    float omega = -asin(objSpaceIntersection.y / 0.5f);
    v = (omega / M_PI) + 0.5f;
    return glm::vec2(u, v);
}

glm::vec2 RayScene::getUVPlane(glm::vec4 objSpaceIntersection, int plane){
    glm::vec2 uv;
    switch (plane) {
    // Front face
    case 1:
        uv = glm::vec2(objSpaceIntersection.x + 0.5f, -objSpaceIntersection.y + 0.5f);
        break;
    // Right face
    case 2:
        uv = glm::vec2(-objSpaceIntersection.z + 0.5f, -objSpaceIntersection.y + 0.5f);
        break;
    // Back face
    case 3:
        uv = glm::vec2(-objSpaceIntersection.x + 0.5f, -objSpaceIntersection.y + 0.5f);
        break;
    // Left face
    case 4:
        uv = glm::vec2(objSpaceIntersection.z + 0.5f, -objSpaceIntersection.y + 0.5f);
        break;
    // Top face
    case 5:
        uv = glm::vec2(objSpaceIntersection.x + 0.5f, objSpaceIntersection.z + 0.5f);
        break;
    // Bottom face
    case 6:
        uv = glm::vec2(objSpaceIntersection.x + 0.5f, -objSpaceIntersection.z + 0.5f);
        break;
    }
    return uv;
}

glm::vec2 RayScene::getTiledCoordinates(glm::vec2 uv, float j, float k, int width, int height)
{
    float s = static_cast<int>(uv.x * j * width) % width;
    float t = static_cast<int>(uv.y * k * height) % height;
    return glm::vec2(s, t);
}



