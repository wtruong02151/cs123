#ifndef RAYSCENE_H
#define RAYSCENE_H

#include "Scene.h"
#include "CS123SceneData.h"
#include "Canvas2D.h"


/**
 * @class RayScene
 *
 *  Students will implement this class as necessary in the Ray project.
 */
class RayScene : public Scene
{
public:
    // Takes in the list of transformed objects in the scene.
    RayScene(std::vector<transformedObject> sceneObjs, std::vector<CS123SceneLightData> lights, CS123SceneGlobalData global);
    virtual ~RayScene();

    // Function called to filter image (used in multithreading).
    void filterImage(Canvas2D* canvas, Camera *camera, int width, int height, int startingRow, int endingRow);

    // Takes in a ray and shoots it towards the scene,
    // calculating if it intersects with any
    glm::vec3 shootRay(CS123Ray ray, int numReflections, bool shadowRecur, int row, int col, int width, int height);

private:

    // Calculates final lighting with lighting equation
    // and returns a vec3 for each color (x, y, z = r, g, b).
    glm::vec3 finalIntensity(CS123ScenePrimitive prim, glm::vec4 pWorldInter,
                             glm::vec3 surfaceNormal, glm::vec4 rayDirection,
                             QColor textureData, float blend);

    // Given an intersection point (x, y, z) and the shape that our ray just intersected,
    // and returns the normal on the shape at that point.
    glm::vec3 getNormal(glm::vec4 intersectionPoint, PrimitiveType shape, glm::mat4x4 transMat, int face);

    // Functions used to calculated value of t for each implicit equation
    // of the shapes. Takes in the point and direction of a given ray
    // to see if the ray intserects the shape.
    glm::vec2 implicitCube(glm::vec4 point, glm::vec4 direction);
    // Returns both the t value and a flag for if we intersected the caps (to calculate proper normal).
    glm::vec2 implicitCone(glm::vec4 point, glm::vec4 direction);
    glm::vec2 implicitCylinder(glm::vec4 point, glm::vec4 direction);
    float implicitSphere(glm::vec4 point, glm::vec4 direction);
    float intersectPlane(glm::vec4 normal, glm::vec4 plane, glm::vec4 point, glm::vec4 direction);

    // Function that takes in an object space intersection and calculates the proper UV-coordinates.
    glm::vec2 getUVCube(glm::vec4 objSpaceIntersection, int plane);
    glm::vec2 getUVConeCylinder(glm::vec4 objSpaceIntersection, int plane);
    glm::vec2 getUVSphere(glm::vec4 objSpaceIntersection);

    // Function that returns the UV coordinates of the object space intersection and a given plane
    // (switch statement to handle incrementaiton of UV coordinates per plane).
    glm::vec2 getUVPlane(glm::vec4 objSpaceIntersection, int plane);

    // Returns the tiled texture coordinate corresponding
    glm::vec2 getTiledCoordinates(glm::vec2 uv, float row, float col, int width, int height);

    // HashMpa that stores loaded images mapped to their filenames.
    QHash<QString, QImage> m_textureHashMap;
};

#endif // RAYSCENE_H
