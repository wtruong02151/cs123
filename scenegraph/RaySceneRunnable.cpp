#include "RaySceneRunnable.h"

RaySceneRunnable::RaySceneRunnable(RayScene *rayScene, Canvas2D *canvas, Camera *camera, int width, int height, int startingRow, int endingRow)
{
    m_rayScene = rayScene;
    m_canvas = canvas;
    m_camera =  camera;
    m_width = width;
    m_height = height;
    m_startingRow = startingRow;
    m_endingRow = endingRow;
}

void RaySceneRunnable::run()
{
    m_rayScene->filterImage(m_canvas, m_camera, m_width, m_height, m_startingRow, m_endingRow);
}
