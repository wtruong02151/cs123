#include "Scene.h"
#include "Camera.h"
#include "CS123SceneData.h"
#include "CS123ISceneParser.h"
#include <QtGlobal>
#include <queue>


Scene::Scene() //: m_camera(NULL)
{

}

Scene::~Scene()
{
    // Do not delete m_camera, it is owned by SupportCanvas3D

}

void Scene::parse(Scene *sceneToFill, CS123ISceneParser *parser)
{
    // TODO: load scene into sceneToFill using setGlobal(), addLight(), addPrimitive(), and
    //   finishParsing()

    // Loop through lights from parser
    CS123SceneLightData light;
    for (int i = 0; i < parser->getNumLights(); i++) {
        if (parser->getLightData(i, light)) {
            sceneToFill->addLight(light);
        }
    }

    // Set global data.
    CS123SceneGlobalData globalData;
    if (parser->getGlobalData(globalData)) {
        sceneToFill->setGlobal(globalData);
    }


    // Grab root then recursively loop through tree.
    CS123SceneNode* root = parser->getRootNode();
    recurTraversal(sceneToFill, *root, glm::mat4(1.0));
}

// Function that performs a dfs to traverse the graph.
void Scene::recurTraversal(Scene *sceneToFill, CS123SceneNode &node, glm::mat4x4 prevTrans)
{
    std::vector<CS123SceneNode*> currChildren = node.children;
    std::vector<CS123SceneTransformation*> currTransformations = node.transformations;
    std::vector<CS123ScenePrimitive*> currPrimitives = node.primitives;

    glm::mat4x4 matrixToPass = prevTrans;

    // Grab transformation of current node to be passed down to children.
    for (int i = 0; i < currTransformations.size(); i++) {
        switch(currTransformations[i]->type) {
        case (TRANSFORMATION_TRANSLATE):
            matrixToPass *= glm::translate(currTransformations[i]->translate);
            break;
        case (TRANSFORMATION_SCALE):
            matrixToPass *= glm::scale(currTransformations[i]->scale);
            break;
        case (TRANSFORMATION_ROTATE):
            matrixToPass *= glm::rotate(currTransformations[i]->angle, currTransformations[i]->rotate);
            break;
        case (TRANSFORMATION_MATRIX):
            matrixToPass *= currTransformations[i]->matrix;
            break;
        }
    }
    // Loop through list of primitives and add them to the vectors.
    for (int i = 0; i < currPrimitives.size(); i++) {
        sceneToFill->addPrimitive(*currPrimitives[i], matrixToPass);
    }
    // Recursively traverse through children
    for (int i = 0; i < currChildren.size(); i++) {
        recurTraversal(sceneToFill, *currChildren[i], matrixToPass);
    }
}


void Scene::addPrimitive(const CS123ScenePrimitive &scenePrimitive, const glm::mat4x4 &matrix)
{
    // Create the struct that we want to push to our list of transformedObjects and
    // push it back onto the list.

    CS123ScenePrimitive primitive = scenePrimitive;

    primitive.material.cAmbient.r *= m_globalData.ka;
    primitive.material.cAmbient.g *= m_globalData.ka;
    primitive.material.cAmbient.b *= m_globalData.ka;

    primitive.material.cDiffuse.r *= m_globalData.kd;
    primitive.material.cDiffuse.g *= m_globalData.kd;
    primitive.material.cDiffuse.b *= m_globalData.kd;

    CS123SceneFileMap *textureMap = new CS123SceneFileMap(*primitive.material.textureMap);

    transformedObject toAdd(primitive, matrix, textureMap);
    m_sceneObjects.push_back(toAdd);
}

void Scene::addLight(const CS123SceneLightData &sceneLight)
{
    // TODO
    m_lights.push_back(sceneLight);
}

void Scene::setGlobal(const CS123SceneGlobalData &global)
{
    m_globalData = global;

}

// Function that returns the vector of primitives stored in the scene.
std::vector<transformedObject> Scene::getTransObjs()
{
    return  m_sceneObjects;
}

// Returns the vector of lights stored in the scene.
std::vector<CS123SceneLightData> Scene::getLights()
{
    return m_lights;
}

CS123SceneGlobalData Scene::getGlobal()
{
    return m_globalData;
}
