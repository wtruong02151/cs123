William Truong
wtruong
9/17/15
CS1230: Filter README

********************
*   DESIGN SPECS   *
********************

For this project, I created a helper function recurTraversal that recursively traversed the scene graph. Scene.h maintains a member variable called m_sceneObjects that stores a list of a struct defidne as transformedObject that holds a composite transformation matrix and the primitive that the matrix gets applied to. As the recursive function searches the tree, the transformation matricies get passed down the branch and are applied to the proper object nodes.Scene.h also maintains a vector for scenelights, which then get set in SceneviewScene.cpp. 

In SceneviewScene.cpp, renderGeometry() functions by looping through the objects to be drawn in m_sceneObjects, applying their respective material settigns and transformations, then drawing the shapes.

******************
*   KNOWN BUGS   *
******************

No known bugs.