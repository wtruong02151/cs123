/**
 * @file   Brush.cpp
 *
 * Implementation of a brush that smudges the image on the canvas as the mouse is dragged.
 *
 * You should fill this file in for the Brush assignment.
 */

#include <CS123Common.h>
#include <cmath>

#include "SmudgeBrush.h"
#include "Canvas2D.h"
#include "Settings.h"


SmudgeBrush::SmudgeBrush(BGRA color, int flow, int radius) : QuadraticBrush(color, flow, radius)
{
    // @TODO: [BRUSH] Initialize any memory you are going to use here. Hint - you are going to
    //       need to store temporary image data in memory. This might be a good place to
    //       allocate that memory.

    // Define buffer to be size of radius.
    tempBuffer = new BGRA[(2*m_radius+1)*(2*m_radius+1)];
    makeMask();
}


SmudgeBrush::~SmudgeBrush()
{
    // @TODO: [BRUSH] Be sure not to leak memory!
    delete tempBuffer;
}

// Deleted make mask since this class inherits from QuadraticBrush.

//! Picks up paint from the canvas before drawing begins.
void SmudgeBrush::pickUpPaint(int x, int y, Canvas2D* canvas)
{
    // @TODO: [BRUSH] Perform the "pick up paint" step described in the assignment handout here.
    //        In other words, you should store the "paint" under the brush mask in a temporary
    //        buffer (which you'll also have to figure out where and how to allocate). Then,
    //        in the paintOnce() method, you'll paste down the paint that you picked up here.
    int width = canvas->width();
    int height = canvas->height();
    BGRA* pix = canvas->data();
    int radius = settings.brushRadius;

    // Grab the starting row/col from the point where the user clicked
    // on the canvas.
    int rowstart = max(y - radius, 0);
    int rowend = min(y + radius + 1, height);
    int colstart = max(x - radius, 0);
    int colend = min(x + radius + 1, width);

    // Initialize variables.
    int rowcounter, colcounter, bufferIndex, canvasIndex, bufferX, bufferY;

    // Calculate the offset indices for the buffer array (same logic for mask array).
    int bufferOffsetRow = radius - (y - rowstart);
    int bufferOffsetCol = radius - (x - colstart);

    for (rowcounter = rowstart; rowcounter < rowend; rowcounter++) {
        for (colcounter = colstart; colcounter < colend; colcounter++) {
            canvasIndex = (width * rowcounter) + colcounter;

            // Increment offset values alongside with loop.
            bufferX = bufferOffsetCol + (colcounter - colstart);
            bufferY = bufferOffsetRow + (rowcounter - rowstart);

            // Calculate the buffer index.
            bufferIndex = ((((2*radius) + 1) * bufferY)) + bufferX;
            tempBuffer[bufferIndex] = pix[canvasIndex];
            tempBuffer[bufferIndex].a = 255;

        }
    }
}

void SmudgeBrush::paintOnce(int mouse_x, int mouse_y, Canvas2D* canvas)
{
    // @TODO: [BRUSH] Here, you'll need to paste down the paint you picked up in
    //        the previous method. Be sure to take the mask into account! You can
    //        ignore the alpha parameter, but you can also use it (smartly) if you
    //        would like to.

    BGRA* pix = canvas->data();

    int radius = settings.brushRadius;

    int rowstart = max(mouse_y - radius, 0);
    int rowend = min(mouse_y + radius + 1, canvas->height());
    int colstart = max(mouse_x - radius, 0);
    int colend = min(mouse_x + radius + 1, canvas->width());

    int rowcounter, colcounter, index, maskX, maskY, maskIndex;

    // Calculate the offset indices for the mask array.
    int maskOffsetRow = radius - (mouse_y - rowstart);
    int maskOffsetCol = radius - (mouse_x - colstart);

    for (rowcounter = rowstart; rowcounter < rowend; rowcounter++) {
        for (colcounter = colstart; colcounter < colend; colcounter++) {
            index = (canvas->width() * rowcounter) + colcounter;

            // Increment offset values alongside with loop.
            maskX = maskOffsetCol + (colcounter - colstart);
            maskY = maskOffsetRow + (rowcounter - rowstart);

            // Calculate the mask index. (same index for the buffer as well).
            maskIndex = ((((2*radius) + 1) * maskY)) + maskX;

            float maskValue = m_mask[maskIndex];

            // Set canvas values to new blended paint values of smudge.
            pix[index].r = blendPaint(pix[index].r, tempBuffer[maskIndex].r, maskValue, 1);
            pix[index].g = blendPaint(pix[index].g, tempBuffer[maskIndex].g, maskValue, 1);
            pix[index].b = blendPaint(pix[index].b, tempBuffer[maskIndex].b, maskValue, 1);

        }
    }
    pickUpPaint(mouse_x, mouse_y, canvas);
}
