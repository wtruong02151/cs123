/**
 * @file   Brush.cpp
 *
 * Implementation of common functionality of bitmap brushes.
 *
 * You should fill this file in while scompleting the Brush assignment.
 */

#include "Brush.h"
#include "Canvas2D.h"
#include "Settings.h"

unsigned char lerp(unsigned char a, unsigned char b, float percent)
{
    float fa = (float)a / 255.0f;
    float fb = (float)b / 255.0f;
    return (unsigned char)((fa + (fb - fa) * percent) * 255.0f + 0.5f);
}



Brush::Brush(BGRA color, int flow, int radius) {

    // @TODO: [BRUSH] Initialize any memory you are going to use here. Hint - you are going to
    //        need to store the mask in memory. This might (or might not) be a good place to
    //        allocate that memory.

    // Example code: (feel free to use)
    m_color = color;
    m_flow = flow;
    m_radius = radius;

    m_mask = NULL;
}


Brush::~Brush()
{
    // @TODO: [BRUSH] Don't forget to delete any memory you allocate. Use delete[] to delete
    //        a whole array. Otherwise you'll just delete the first element!
    //
    //        i.e. delete[] m_mask;
    //

    delete[] m_mask;
}


void Brush::setGreen(int green)
{
    m_color.g = green;
}


void Brush::setRed(int red)
{
    m_color.r = red;
}


void Brush::setBlue(int blue)
{
    m_color.b = blue;
}


void Brush::setFlow(int flow)
{
    m_flow = flow;
    makeMask();
}


void Brush::setRadius(int radius)
{
    m_radius = radius;
    makeMask();
}

void Brush::pickUpPaint(int x, int y, Canvas2D *canvas)
{
    // Empty function, not called for any other brushes except SmudgeBrush (overwritten
    // in that subclass).
}

void Brush::paintOnce(int mouse_x, int mouse_y, Canvas2D* canvas)
{
    // @TODO: [BRUSH] You can do any painting on the canvas here. Or, you can
    //        override this method in a subclass and do the painting there.
    //
    // Example: You'll want to delete or comment out this code, which
    // sets all the pixels on the canvas to red.
    //

    std::cout<<"here"<<endl;
    BGRA* pix = canvas->data();
//    int size = canvas->width() * canvas->height();

    int radius = settings.brushRadius;

    // Calculate indices; ensure that only values on the canvas are grabbed,
    // not null values out of bounds of the canvas.

    int rowstart = max(mouse_y - radius, 0);
    int rowend = min(mouse_y + radius + 1, canvas->height());
    int colstart = max(mouse_x - radius, 0);
    int colend = min(mouse_x + radius + 1, canvas->width());

    int rowcounter, colcounter, index, maskX, maskY, maskIndex;

    // Calculate the offset indices for the mask array.
    int maskOffsetRow = radius - (mouse_y - rowstart);
    int maskOffsetCol = radius - (mouse_x - colstart);

    for (rowcounter = rowstart; rowcounter < rowend; rowcounter++) {
        for (colcounter = colstart; colcounter < colend; colcounter++) {
            index = (canvas->width() * rowcounter) + colcounter;

            // Increment offset values alongside with loop.
            maskX = maskOffsetCol + (colcounter - colstart);
            maskY = maskOffsetRow + (rowcounter - rowstart);

            // Calculate the mask index.
            maskIndex = ((((2*radius) + 1) * maskY)) + maskX;

            float maskValue = m_mask[maskIndex]/255.0f; // Mask value stored from 0-255

            // Set canvas values to new blended paint values.
            pix[index].r = blendPaint(pix[index].r, m_color.r, maskValue, m_flow);
            pix[index].g = blendPaint(pix[index].g, m_color.g, maskValue, m_flow);
            pix[index].b = blendPaint(pix[index].b, m_color.b, maskValue, m_flow);
        }
    }
}

// Function that calculates new BGRA values appropriatley based on brush colors and canvas colors.
float Brush::blendPaint(float canvasColor, float brushColor, float maskValue, float alpha)
{
    return (brushColor * alpha * maskValue) + ((1 - (maskValue * alpha)) * canvasColor);
}
