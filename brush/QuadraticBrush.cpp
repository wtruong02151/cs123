/**
 * @file   QuadraticBrush.cpp
 *
 * Implementation of a brush with a quadratic mask distribution.
 *
 * You should fill this file in while completing the Brush assignment.
 */

#include "QuadraticBrush.h"
#include <cmath>

QuadraticBrush::QuadraticBrush(BGRA color, int flow, int radius)
    : Brush(color, flow, radius)
{
    // @TODO: [BRUSH] You'll probably want to set up the mask right away.
    makeMask();
}

QuadraticBrush::~QuadraticBrush()
{
    // @TODO: [BRUSH] Delete any resources owned by this brush, so you don't leak memory.

}

void QuadraticBrush::makeMask()
{
    // @TODO: [BRUSH] Set up the mask for your Quadratic brush here...
    int width = 2*m_radius + 1;

    // Point for center.
    int center = m_radius;

    // Initialize variables.
    int index;
    float totDist;

    // Set mask dimensions equal to the radius of the brush.
    m_mask = new float[width * width];

    // Set mask values based on constant distributions.
    for (int x = 0; x < width; x++) {
        for (int y = 0; y < width; y++) {
            totDist = sqrt(pow(x - center, 2) + pow(y - center, 2));
            index = (width * x) + y;
            // All pixels with a distance within the radius from
            // from the center have a mask value equal to a quadratically decreasing
            // amount from the center. The further away a pixel away is from the center,
            // the smaller the mask value.
            if (totDist <= m_radius) {
                m_mask[index] = pow(1.0 - (totDist / m_radius), 2);
            }
            // Outside of radius, set mask value to 0.
            else {
                m_mask[index] = 0;
            }

        }
    }
}


