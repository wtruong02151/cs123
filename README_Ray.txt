William Truong
wtruong
11/24/15
CS1230: Ray README

********************
*   DESIGN SPECS   *
********************

A note to start: I managed to fix the specs of black dots when found on various surfaces of my cubes when rendering the scene (a known bug when I submitted Intersect). The issue I found was with floating point precision. When calculating normals for each face, I was simply checking if a particular coordinate was equal to 0.5f or -0.5f (to check which plane we intersected), and then return the corresponding normal vector. Some coordinate values came out to be 0.4999999f or something like that. I adjusted my implicit functions to return an integer corresponding to which face we intersected (more details in the comments of my code). This made checking for which face we hit very accurate as opposed to my previous implementation.

Most modifications were made to my RayScene.cpp file. I modified part of the CS123SceneData.h file to accomodate for loading up the texture map (I created a constructor for CS123SceneFileMap that copied the proper values of CS123SceneFileMap and stored them, and stored this in transformedObject struct generated in my SceneviewScene.cpp).

Reflections were implemented by modifying my shootRay() function. For each initial ray shot, if we intersect with an object, calculate the reflection vector, generate a new ray and recursively shoot this ray in the direction of the reflection vector. I've set a parameter that the TAs can modify that determines how many reflection bounces should be taken into account when calculating the color of the original object at that point.

Shadows were implemented by modifying my shootRay() function as well. For each intersection point from when we shoot our initial rays, and for each light source, shoot a new ray towards the light source and check if there's an object obstructing the light. The edge-cases that are accounted for include when our recursive ray hits an object beyond our light source, then be sure to include this light since that object technically does not obstruct the light source.

I created new functions that, given an object space coordinate, returns the proper UV-coordinate for that particular shape. These functions are located at the bottom of RayScene.cpp.

My finalIntensity() function which is used to calculate the lighting equation was also modified to take into account specular lighting,attenuation, as well as the effects of reflection, shadows and texture mapping (see comments in code for more details).

******************
*   KNOWN BUGS   *
******************

No known bugs. Although when compared to the cs123_demo, my rendering seems to be slightly darker. Not too sure why this is the problem. I could be multiplying by the coefficients too often, but again, I'm not entirely sure.

********************
*   EXTRA CREDIT   *
********************

I implemented multi-threading as well as the use of a HashMap for loading textures to quicken up rendering a scene.