#ifndef CONE_H
#define CONE_H

#include "shapes/shapes.h"
#include "OpenGLScene.h"
#include <math.h>

class cone : public shapes
{
public:
    // Cube constructor; sets m_numStrips (param1), m_length (1),
    // and m_offset (m_length / m_numStrips);
    cone(int param1, int param2, GLint shader, NormalRenderer* normalRenderer);

    // Super class function (overwritten) used to generate and return
    // the vertices for the shape.
    virtual void generateVertices();

private:
    // Converts from polar coordinates to cartesian coordinates.
    float convertX(float radius, float theta);
    float convertZ(float radius, float theta);

    // Creates the cap for the cone. makeCap() takes in a starting
    // y-coordinate as the height of the cone, and a posNeg value to determine
    // when to flip the coordinates while generating the bands of triangles
    // to make them appear properly on the 3D canvas. This function
    // loops through both the number of stacks and slices to generate the proper
    // coordinates for each vertex.
    void makeCap(float myY, int posNeg);

    // Create the top set of triangles (pyramid of isoceles triangles). Loops through
    // numver of slices to generate each side of the pyramid properly.
    void makeTopCone(float maxY);

    // Creates the rest of the cone.
    void makeBottomCone();

    // Global variables of the num of stacks and slices for the current
    // generation of the cone.
    int m_stacks, m_slices;

    // m_radius is a global variable for the radius of the cone (0.5).
    // m_height is a gloval variable for the height of the cone (0.5).
    // m_stackIncrems is the incrementation along the height of the
    // cone, based on  how many stacks the shape should have (param1).
    // m_radiusIncrems is the incrementation along the radius of the
    // cone, based on how many bands the shape should have.
    // m_sliceAngles is the degrees in which the cone is divided into,
    // determined by the value of param2.
    float m_radius, m_height, m_stackIncrems, m_radiusIncrems, m_sliceAngles;
};

#endif // CONE_H
