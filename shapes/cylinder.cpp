#include "cylinder.h"
#include "shapes.h"
cylinder::cylinder(int param1, int param2, GLint shader, NormalRenderer* normalRenderer)
    : shapes(param1, param2, shader, normalRenderer)
{
    param2 = max(3, param2);

    // Set m_stacks and m_slices equal to parameters.
    m_stacks = param1;
    m_slices = param2;

    // Set static m_height and m_radius.
    m_radius = 0.5;
    m_height = 1.0;

    // Incrementation for m_radius for generation of top/bottom
    // faces of cylinder
    m_radiusIncrems = m_radius / m_stacks;

    // Incrementation for m_stacks for generation of barrel
    // of cylinder
    m_stackIncrems = m_height / m_stacks;

    // Slice angles determined by 360* divided by m_slices.
    m_sliceAngles = (2 * M_PI) / m_slices;

    // Call generateVertices().
    generateVertices();
}

void cylinder::generateVertices()
{
    makeCap(0.5, 1);
    makeCap(-0.5, -1);
    makeBarrel();
    shapes::init();
}

void cylinder::makeCap(float myY, int posNeg)
{
    float x, z;
    // Bands
    for (int stack = 0; stack < m_stacks; stack++) {
        // m_m_slices
        for (float sliceAng = 0; sliceAng < (2 * M_PI); sliceAng += m_sliceAngles) {
            // FIRST TRIANGLE
            // Top left point
            x = convertX(m_radiusIncrems * stack, -posNeg*sliceAng);
            z = convertZ(m_radiusIncrems * stack, -posNeg*sliceAng);
            insertPoint(x, myY, z);
            insertPoint(0, posNeg ,0);

            // Bottom left point
            x = convertX(m_radiusIncrems * stack + m_radiusIncrems, -posNeg*(sliceAng));
            z = convertZ(m_radiusIncrems * stack + m_radiusIncrems, -posNeg*(sliceAng));
            insertPoint(x, myY, z);
            insertPoint(0, posNeg, 0);

            // Top right point
            x = convertX(m_radiusIncrems * stack, -posNeg*(sliceAng + m_sliceAngles));
            z = convertZ(m_radiusIncrems * stack, -posNeg*(sliceAng + m_sliceAngles));
            insertPoint(x, myY, z);
            insertPoint(0, posNeg, 0);

            // SECOND TRIANGLE
            // Top right point
            x = convertX(m_radiusIncrems * stack, -posNeg*(sliceAng + m_sliceAngles));
            z = convertZ(m_radiusIncrems * stack, -posNeg*(sliceAng + m_sliceAngles));
            insertPoint(x, myY, z);
            insertPoint(0, posNeg, 0);

            // Bottom left point
            x = convertX(m_radiusIncrems * stack + m_radiusIncrems, -posNeg*(sliceAng));
            z = convertZ(m_radiusIncrems * stack + m_radiusIncrems, -posNeg*(sliceAng));
            insertPoint(x, myY, z);
            insertPoint(0, posNeg, 0);

            // Bottom right point
            x = convertX(m_radiusIncrems * stack + m_radiusIncrems, -posNeg*(sliceAng + m_sliceAngles));
            z = convertZ(m_radiusIncrems * stack + m_radiusIncrems, -posNeg*(sliceAng + m_sliceAngles));
            insertPoint(x, myY, z);
            insertPoint(0, posNeg, 0);
        }
    }
}

void cylinder::makeBarrel()
{
    float x, y, z, startingY;
    glm::vec3 normal;
    // Looping through vertical m_stacks
    for (int stack = 0; stack < m_stacks; stack++) {
        // Looping through m_slices
        for (float sliceAng = 0; sliceAng < (2 * M_PI); sliceAng += m_sliceAngles) {
            startingY = -0.5 + m_stackIncrems * stack;
            // FIRST TRIANGLE
            // Top left point
            x = convertX(m_radius, -1*sliceAng);
            y = startingY + m_stackIncrems;
            z = convertZ(m_radius, -1*sliceAng);
            insertPoint(x, y, z);
            normal = getNormal(-1*sliceAng);
            insertPoint(normal.x, normal.y, normal.z);

            // Bottom left point
            x = convertX(m_radius, -1*sliceAng);
            y = startingY;
            z = convertZ(m_radius, -1*sliceAng);
            insertPoint(x, y, z);
            insertPoint(normal.x, normal.y, normal.z);

            // Top right point
            x = convertX(m_radius, -1*(sliceAng + m_sliceAngles));
            y = startingY + m_stackIncrems;
            z = convertZ(m_radius, -1*(sliceAng + m_sliceAngles));
            insertPoint(x, y, z);
            normal = getNormal(-1*(sliceAng + m_sliceAngles));
            insertPoint(normal.x, normal.y, normal.z);

            // SECOND TRIANGLE
            // Top right point
            x = convertX(m_radius, -1*(sliceAng + m_sliceAngles));
            y = startingY + m_stackIncrems;
            z = convertZ(m_radius, -1*(sliceAng + m_sliceAngles));
            insertPoint(x, y, z);
            insertPoint(normal.x, normal.y, normal.z);

            // Bottom left point
            x = convertX(m_radius, -1*sliceAng);
            y = startingY;
            z = convertZ(m_radius, -1*sliceAng);
            insertPoint(x, y, z);
            normal = getNormal(-1*sliceAng);
            insertPoint(normal.x, normal.y, normal.z);

            // Bottom right point
            x = convertX(m_radius, -1*(sliceAng + m_sliceAngles));
            y = startingY;
            z = convertZ(m_radius, -1*(sliceAng + m_sliceAngles));
            insertPoint(x, y, z);
            normal = getNormal(-1*(sliceAng + m_sliceAngles));
            insertPoint(normal.x, normal.y, normal.z);
        }
    }
}

float cylinder::convertX(float r, float theta)
{
    return r * cos(theta);
}

float cylinder::convertZ(float r, float theta)
{
    return r * sin(theta);
}

glm::vec3 cylinder::getNormal(float theta)
{
    float x = cos(theta);
    float y = 0;
    float z = sin(theta);
    return glm::vec3(x, y, z);
}
