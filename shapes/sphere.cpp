#include "shapes/sphere.h"
#include <math.h>

// Sphere constructor; sets m_radius to 0.5,
// and calculates m_latStackAngles and m_longSliceAngles
// based on param1 and param 2 respectively.
sphere::sphere(int param1, int param2, GLint shader, NormalRenderer* normalRenderer)
    : shapes(param1, param2, shader, normalRenderer)
{

    // Set minimum parameters.
    param1 = max(2, param1);
    param2 = max(3, param2);

    // Radius is 1/2.
    m_radius = 0.5;

    // Angle increments dependent on param1 or param2 (used for sphere generation).
    m_latStackAngles = (M_PI) / param1;
    m_longSliceAngles = (2 * M_PI) / param2;

    generateVertices();
}

// Super class function used to generate and return
// the vertices for the shape.
void sphere::generateVertices()
{
    makeSphere();
    shapes::init();
}

// Function that generates points needed for sphere generation. Loops through
// slice angles and stack angles to determine coordinates of the points for each
// triangle.
void sphere::makeSphere()
{
    float x, y, z;
    glm::vec3 normal;
    // Horizontal angles; loop through each slice angle to calculate points accordingly.
    for (float longAngle = 0; longAngle < (2 * M_PI); longAngle += m_longSliceAngles) {
        // Vertical angles; loop through each stack angle to calculate points accordingly.
        for (float latAngle = 0; latAngle < M_PI; latAngle += m_latStackAngles) {
            // First Triangle
            // Top left
            x = convertX(m_radius, -1*longAngle, -1*latAngle);
            y = convertY(m_radius, -1*latAngle);
            z = convertZ(m_radius, -1*longAngle, -1*latAngle);
            insertPoint(x, y, z);
            normal = getNormal(x, y, z);
            insertPoint(normal.x, normal.y, normal.z);

            // Bottom left
            x = convertX(m_radius, -1*longAngle, -1*(latAngle + m_latStackAngles));
            y = convertY(m_radius, -1*(latAngle + m_latStackAngles));
            z = convertZ(m_radius, -1*longAngle, -1*(latAngle + m_latStackAngles));
            insertPoint(x, y, z);
            normal = getNormal(x, y, z);
            insertPoint(normal.x, normal.y, normal.z);

            // Bottom right
            x = convertX(m_radius, -1*(longAngle + m_longSliceAngles), -1*(latAngle + m_latStackAngles));
            y = convertY(m_radius, -1*(latAngle + m_latStackAngles));
            z = convertZ(m_radius, -1*(longAngle + m_longSliceAngles), -1*(latAngle + m_latStackAngles));
            insertPoint(x, y, z);
            normal = getNormal(x, y, z);
            insertPoint(normal.x, normal.y, normal.z);

            // Right Triangle
            // Top left
            x = convertX(m_radius, -1*longAngle, -1*latAngle);
            y = convertY(m_radius, -1*latAngle);
            z = convertZ(m_radius, -1*longAngle, -1*latAngle);
            insertPoint(x, y, z);
            normal = getNormal(x, y, z);
            insertPoint(normal.x, normal.y, normal.z);

            // Bottom right
            x = convertX(m_radius, -1*(longAngle + m_longSliceAngles), -1*(latAngle + m_latStackAngles));
            y = convertY(m_radius, -1*(latAngle + m_latStackAngles));
            z = convertZ(m_radius, -1*(longAngle + m_longSliceAngles), -1*(latAngle + m_latStackAngles));
            insertPoint(x, y, z);
            normal = getNormal(x, y, z);
            insertPoint(normal.x, normal.y, normal.z);

            // Top right
            x = convertX(m_radius, -1*(longAngle + m_longSliceAngles), -1*(latAngle));
            y = convertY(m_radius, -1*latAngle);
            z = convertZ(m_radius, -1*(longAngle + m_longSliceAngles), -1*latAngle);
            insertPoint(x, y, z);
            normal = getNormal(x, y, z);
            insertPoint(normal.x, normal.y, normal.z);
        }
    }
}

// Takes in a radius, a longitude and a latitude to convert the spherical
// x-component into a cartesian x coordinate.
float sphere::convertX(float r, float longitude, float latitude)
{
    return r * sin(latitude) * cos(longitude);
}

// Takes in a radius and a latitude to convert the spherical
// y-component into a cartesian y coordinate.
float sphere::convertY(float r, float latitude)
{
    return r * cos(latitude);
}

// Takes in a radius, a longitude and a latitude to convert the spherical
// z-component into a cartesian z coordinate.
float sphere::convertZ(float r, float longitude, float latitude)
{
    return r * sin(latitude) * sin(longitude);
}

// For a given vertex, return its normalized vector (normal to the
// surface of the sphere).
glm::vec3 sphere::getNormal(float x, float y, float z)
{
    glm::vec3 currVec = glm::vec3(x, y, z);
    glm::vec3 normalVec = glm::normalize(currVec);
    return normalVec;
}
