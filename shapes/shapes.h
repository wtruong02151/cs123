#ifndef SHAPES_H
#define SHAPES_H

#include "OpenGLScene.h"
#include <math.h>

class shapes
{
public:
    shapes(int param1, int param2, GLint shader, NormalRenderer* normalRenderer);

    // Initializer for shapes; handles all functionality related to
    // VBO as well as the m_normalRenderer from the ShapeScene.cpp.
    void init();

    // Function used to draw the VAO. Made in this function
    // since the size of m_vertexData must be known to draw the array.
    void draw();

private:
    // Member variables for shader and normal renderer (passed in
    // by ShapeScene.cpp.
    GLint m_shader;
    NormalRenderer* m_normalRenderer;

    // Store size of m_vertexDataSize.
    int m_vertexDataSize;

    // These member variables and functions are protected
    // so that subclasses have access to them.
protected:
    // Create function to generate vertices. Overwritten by each
    // shape since each shape generates its vertices in a unique way.
    virtual void generateVertices() = 0;

    // Inserts point into a vector that's used as the buffer data array.
    void insertPoint(float x, float y, float z);

    // Dynamic array of vertices.
    std::vector<GLfloat> m_vertexData;

    GLuint m_vaoID; // The vertex array object ID, which is passed to glBindVertexArray.

};

#endif // SHAPES_H
