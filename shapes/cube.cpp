#include "cube.h"

Cube::Cube(int param1, int param2, GLint shader, NormalRenderer* normalRenderer)
    : shapes(param1, param2, shader, normalRenderer)
{
    // Number of strips (equal to p1).
    m_numStrips = param1;

    // Global variable for m_length.
    m_length = 1;

    // m_offsets determined by m_length of cube divided by
    // number of strips needed to generate.
    m_offset = m_length / m_numStrips;
    generateVertices();
}

void Cube::generateVertices()
{
    //
    makeCube(-0.5, 0.5, 0.5);
    shapes::init();
}

void Cube::makeCube(float x, float y, float z)
{
    // +XY plane (front face)
    makeFace(x, y, z, 0);

    // -ZY plane (left face)
    makeFace(z - m_length, y, x, 1);

    // +XZ plane (top face)
    makeFace(x, z - m_length, y, 2);

    // -XY plane (back face)
    makeFace(x + m_offset, y, z - m_length, 3);

    // +ZY plane (right face)
    makeFace(z - (m_numStrips - 1)*m_offset, y, x + m_length, 4);

    // -XZ plane (bottom face)
    makeFace(x + m_offset, z - m_length, -y, 5);
}

void Cube::makeFace(float horzCoor, float vertCoor, float constant, int face)
{
   // Make the proper amount of strips we need for this face.
   for (int i = 0; i < m_numStrips; i ++) {
       makeStrip(horzCoor + m_offset*i, vertCoor, constant, face);
   }
}


void Cube::makeStrip(float horzCoor, float vertCoor, float constant, int face)
{
    // Strips to a face (equal to param1);
    // Number of squares per strip is equal to the number
    // of strips needed to make.
    for (int i = 0; i < m_numStrips + 0; i++) {
        if (face == 2 || face == 5) {
            makeSquare(horzCoor, vertCoor + m_offset*i, constant, face, i);
        }
        else {
            makeSquare(horzCoor, vertCoor - m_offset*i, constant, face, i);
        }
    }
}

void Cube::makeSquare(float horzCoor, float vertCoor, float constant, int face, int i)
{
    switch (face) {
    // Front face (XY Plane), so horzCoor = x, vertCoor = y, constant = z;
    case 0:
        // LEFT TRIANGLE.
        // Top left point
        insertPoint(horzCoor, vertCoor, constant);
        insertPoint(0, 0, 1);
        // Bottom left point
        insertPoint(horzCoor, vertCoor - m_offset, constant);
        insertPoint(0, 0, 1);
        // Bottom right point
        insertPoint(horzCoor + m_offset, vertCoor - m_offset, constant);
        insertPoint(0, 0, 1);

        // RIGHT TRIANGLE.
        // Top left point
        insertPoint(horzCoor, vertCoor, constant);
        insertPoint(0, 0, 1);
        // Bottom right point
        insertPoint(horzCoor + m_offset, vertCoor - m_offset, constant);
        insertPoint(0, 0, 1);
        // Top right point
        insertPoint(horzCoor + m_offset, vertCoor, constant);
        insertPoint(0, 0, 1);
        break;
    // Left face (ZY plane), so horzCoor = z, vertCoor = y, constant = x;
    case 1:
        // LEFT TRIANGLE.
        // Top left point
        insertPoint(constant, vertCoor, horzCoor);
        insertPoint(-1, 0, 0);
        // Bottom left point
        insertPoint(constant, vertCoor - m_offset, horzCoor);
        insertPoint(-1, 0, 0);
        // Bottom right point
        insertPoint(constant, vertCoor - m_offset, horzCoor + m_offset);
        insertPoint(-1, 0, 0);

        // RIGHT TRIANGLE.
        // Top left point
        insertPoint(constant, vertCoor, horzCoor);
        insertPoint(-1, 0, 0);
        // Bottom right point
        insertPoint(constant, vertCoor - m_offset, horzCoor + m_offset);
        insertPoint(-1, 0, 0);
        // Top right point
        insertPoint(constant, vertCoor, horzCoor + m_offset);
        insertPoint(-1, 0, 0);
        break;

    // Top face (XZ plane), so horzCoor = x, vertCoor = z, constant = y;
    case 2:
        // LEFT TRIANGLE.
        // Top left point
        insertPoint(horzCoor, constant, vertCoor);
        insertPoint(0, 1, 0);
        // Bottom left point
        insertPoint(horzCoor, constant, vertCoor + m_offset);
        insertPoint(0, 1, 0);
        // Bottom right point
        insertPoint(horzCoor + m_offset, constant, vertCoor + m_offset);
        insertPoint(0, 1, 0);

        // Top left point
        insertPoint(horzCoor, constant, vertCoor);
        insertPoint(0, 1, 0);
        // Bottom right point
        insertPoint(horzCoor + m_offset, constant, vertCoor + m_offset);
        insertPoint(0, 1, 0);
        // Top right point
        insertPoint(horzCoor + m_offset, constant, vertCoor);
        insertPoint(0, 1, 0);
        break;

    // Back face (XY Plane), so horzCoor = x, vertCoor = y, constant = z;
    case 3:
        insertPoint(horzCoor, vertCoor, constant);
        insertPoint(0, 0, -1);
        // Bottom left point
        insertPoint(horzCoor, vertCoor - m_offset, constant);
        insertPoint(0, 0, -1);
        // Bottom right point
        insertPoint(horzCoor - m_offset, vertCoor - m_offset, constant);
        insertPoint(0, 0, -1);

        // RIGHT TRIANGLE.
        // Top left point
        insertPoint(horzCoor, vertCoor, constant);
        insertPoint(0, 0, -1);
        // Bottom right point
        insertPoint(horzCoor - m_offset, vertCoor - m_offset, constant);
        insertPoint(0, 0, -1);
        // Top right point
        insertPoint(horzCoor - m_offset, vertCoor, constant);
        insertPoint(0, 0, -1);
        break;

    // Right face (-ZY plane), horzCoor = x, vertCoor = z, constant = y;
    case 4:
        // LEFT TRIANGLE.
        // Top left point
        insertPoint(constant, vertCoor, horzCoor);
        insertPoint(1, 0, 0);
        // Bottom left point
        insertPoint(constant, vertCoor - m_offset, horzCoor);
        insertPoint(1, 0, 0);
        // Bottom right point
        insertPoint(constant, vertCoor - m_offset, horzCoor - m_offset);
        insertPoint(1, 0, 0);

        // RIGHT TRIANGLE.
        // Top left point
        insertPoint(constant, vertCoor, horzCoor);
        insertPoint(1, 0, 0);
        // Bottom right point
        insertPoint(constant, vertCoor - m_offset, horzCoor - m_offset);
        insertPoint(1, 0, 0);
        // Top right point
        insertPoint(constant, vertCoor, horzCoor - m_offset);
        insertPoint(1, 0, 0);
        break;

    // Bottom face (ZX plane), horzCoor = x, vertCoor = z, constant = y;
    case 5:
        // LEFT TRIANGLE.
        // Top left point
        insertPoint(horzCoor, constant, vertCoor);
        insertPoint(0, -1, 0);
        // Bottom left point
        insertPoint(horzCoor, constant, vertCoor + m_offset);
        insertPoint(0, -1, 0);
        // Bottom right point
        insertPoint(horzCoor - m_offset, constant, vertCoor + m_offset);
        insertPoint(0, -1, 0);

        // Top left point
        insertPoint(horzCoor, constant, vertCoor);
        insertPoint(0, -1, 0);
        // Bottom right point
        insertPoint(horzCoor - m_offset, constant, vertCoor + m_offset);
        insertPoint(0, -1, 0);
        // Top right point
        insertPoint(horzCoor - m_offset, constant, vertCoor);
        insertPoint(0, -1, 0);
        break;
    }
}
