#ifndef CUBE_H
#define CUBE_H

#include "shapes/shapes.h"

class Cube : public shapes
{
public:
    // Cube constructor; sets m_numStrips (param1), m_length (1),
    // and m_offset (m_length / m_numStrips);
    Cube(int param1, int param2, GLint shader, NormalRenderer* normalRenderer);

    // Super class function (overwritten) used to generate and return
    // the vertices for the shape.
    virtual void generateVertices();

private:
    // Number of strips needed
    int m_numStrips;
    // m_offset is used to determine howm_length is set equal to length of cube (1).
    float m_offset, m_length;

    // Makes the cube (calls makeFace() 6 times, once for each face). Takes in starting
    // points for top-left most point. All faces are generated
    // by starting at the top-left most point on each face.
    void makeCube(float x, float y, float z);

    // Make a face of the cube (composed of strips). Takes in a horizontal coordinate,
    // vertical coordinate, a constant coordinate, and the face of the cube we're currently on
    // to generate the vertices needed to create that face. Loops through the number
    // of strips the face has (param1), and calls makeStrip().
    void makeFace(float horzCoor, float vertCoor, float constant, int face);

    // Make a strip of the cube (composed of strips). Takes in a horizontal coordinate,
    // vertical coordinate, a constant coordinate, and the face of the cube we're currently on
    // to generate the vertices needed to create that face. Loops through the number
    // of square the strip has (param1), and calls makeSquare().
    void makeStrip(float horzCoor, float vertCoor, float constant, int face);

    // Make a square (composed of 2 triangles) on the plane based on int face
    // (switch statement).
    void makeSquare(float horzCoor, float vertCoor, float constant, int face, int i);

};

#endif // CUBE_H
