#include "shapes.h"

shapes::shapes(int param1, int param2, GLint shader, NormalRenderer* normalRenderer)
{
    m_shader = shader;
    m_normalRenderer = normalRenderer;
}

// Initializer for shapes; handles all functionality related to
// VBO as well as the m_normalRenderer from the ShapeScene.cpp.
void shapes::init()
{

    m_vertexDataSize = m_vertexData.size();

    // Initialize the vertex array object.
    glGenVertexArrays(1, &m_vaoID);
    glBindVertexArray(m_vaoID);

    // Initialize the vertex buffer object.
    GLuint vertexBuffer;
    glGenBuffers(1, &vertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);


    // Pass vertex data to OpenGL.
    glBufferData(GL_ARRAY_BUFFER, m_vertexDataSize * sizeof(GLfloat), &m_vertexData[0], GL_STATIC_DRAW);
    glEnableVertexAttribArray(glGetAttribLocation(m_shader, "position"));
    glEnableVertexAttribArray(glGetAttribLocation(m_shader, "normal"));
    glVertexAttribPointer(
        glGetAttribLocation(m_shader, "position"),
        3,                   // Num coordinates per position
        GL_FLOAT,            // Type
        GL_FALSE,            // Normalized
        sizeof(GLfloat) * 6, // Stride
        (void*) 0            // Array buffer offset
    );
    glVertexAttribPointer(
        glGetAttribLocation(m_shader, "normal"),
        3,           // Num coordinates per normal
        GL_FLOAT,    // Type
        GL_TRUE,     // Normalized
        sizeof(GLfloat) * 6,           // Stride
        (void*) (sizeof(GLfloat) * 3)    // Array buffer offset
    );

    // Unbind buffers.
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    // Initialize normals so they can be displayed with arrows. (This can be very helpful
    // for debugging!)
    // This object (m_normalRenderer) can be passed around to other classes,
    // make sure to include "OpenGLScene.h" in any class you want to use the NormalRenderer!
    // generateArrays will take care of any cleanup from the previous object state.
    m_normalRenderer->generateArrays(
                &m_vertexData[0],             // Pointer to vertex data
                6 * sizeof(GLfloat),    // Stride (distance between consecutive vertices/normals in BYTES
                0,                      // Offset of first position in BYTES
                3 * sizeof(GLfloat),    // Offset of first normal in BYTES
                m_vertexDataSize / 6);                // Number of vertices

    // Don't forget to clean up any resources you created
}

// Inserts point into a vector that's used as the buffer data array.
void shapes::insertPoint(float x, float y, float z)
{
    m_vertexData.push_back(x);
    m_vertexData.push_back(y);
    m_vertexData.push_back(z);
}

// Function used to draw the VAO. Made in this function
// since the size of m_vertexData must be known to draw the array.
void shapes::draw()
{
    glBindVertexArray(m_vaoID);
    glDrawArrays(GL_TRIANGLES, 0, m_vertexDataSize / 6 /* Number of vertices to draw */);
    glBindVertexArray(0);
}
