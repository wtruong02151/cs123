#ifndef SPHERE_H
#define SPHERE_H

#include "shapes/shapes.h"
#include "OpenGLScene.h"
#include "ShapesScene.h"
#include <math.h>

class sphere : public shapes
{
public:
    // Sphere constructor; sets m_radius to 0.5,
    // and calculates m_latStackAngles and m_longSliceAngles
    // based on param1 and param 2 respectively.
    sphere(int param1, int param2, GLint shader, NormalRenderer* normalRenderer);

    // Super class function used to generate and return
    // the vertices for the shape.
    virtual void generateVertices();

private:
    // Function that generates points needed for sphere generation. Loops through
    // slice angles and stack angles to determine coordinates of the points for each
    // triangle.
    void makeSphere();

    // Takes in a radius, a longitude and a latitude to convert the spherical
    // x-component into a cartesian x coordinate.
    float convertX(float radius, float longitude, float latitude);

    // Takes in a radius and a latitude to convert the spherical
    // y-component into a cartesian y coordinate.
    float convertY(float radius, float latitude);

    // Takes in a radius, a longitude and a latitude to convert the spherical
    // z-component into a cartesian z coordinate.
    float convertZ(float radius, float longitude, float latitude);

    // For a given vertex, return its normalized vector (normal to the
    // surface of the sphere).
    glm::vec3 getNormal(float x, float y, float z);

    // Member variables for radius of the sphere, the "stack" angles
    // which is determined by the value of param1, and the "slice" angles
    // which is determined by the value of param2.
    float m_radius, m_latStackAngles, m_longSliceAngles;
};

#endif // SPHERE_H
