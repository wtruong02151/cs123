#include "cone.h"
#include "shapes.h"
cone::cone(int param1, int param2, GLint shader, NormalRenderer* normalRenderer)
    : shapes(param1, param2, shader, normalRenderer)
{
    param2 = max(3, param2);

    // Set stacks and slices equal to parameters.
    m_stacks = param1;
    m_slices = param2;

    // Set static height and radius.
    m_radius = 0.5;
    m_height = 1.0;

    // Incrementation for m_radius for generation of bottom
    // face of cone
    m_radiusIncrems = m_radius / m_stacks;

    // Incrementation for m_stacks for generation of pyramid
    // of cone
    m_stackIncrems = m_height / m_stacks;

    // Slice angles determined by 360* divided by slices.
    m_sliceAngles = (2 * M_PI) / m_slices;

    // Call generateVertices().
    generateVertices();
}

void cone::generateVertices()
{
    makeCap(-0.5, -1);
    makeTopCone(0.5);
    makeBottomCone();
    shapes::init();
}

void cone::makeCap(float myY, int posNeg)
{
    float x, z;
    // Loop through m_stacks
    for (int stack = 0; stack < m_stacks; stack++) {
        // Loop through m_slices
        for (float sliceAng = 0; sliceAng < (2 * M_PI); sliceAng += m_sliceAngles) {
            // FIRST TRIANGLE
            // Top left point
            x = convertX(m_radiusIncrems * stack, -posNeg*sliceAng);
            z = convertZ(m_radiusIncrems * stack, -posNeg*sliceAng);
            insertPoint(x, myY, z);
            insertPoint(0, posNeg ,0);

            // Bottom left point
            x = convertX(m_radiusIncrems * stack + m_radiusIncrems, -posNeg*(sliceAng));
            z = convertZ(m_radiusIncrems * stack + m_radiusIncrems, -posNeg*(sliceAng));
            insertPoint(x, myY, z);
            insertPoint(0, posNeg, 0);

            // Top right point
            x = convertX(m_radiusIncrems * stack, -posNeg*(sliceAng + m_sliceAngles));
            z = convertZ(m_radiusIncrems * stack, -posNeg*(sliceAng + m_sliceAngles));
            insertPoint(x, myY, z);
            insertPoint(0, posNeg, 0);

            // SECOND TRIANGLE
            // Top right point
            x = convertX(m_radiusIncrems * stack, -posNeg*(sliceAng + m_sliceAngles));
            z = convertZ(m_radiusIncrems * stack, -posNeg*(sliceAng + m_sliceAngles));
            insertPoint(x, myY, z);
            insertPoint(0, posNeg, 0);

            // Bottom left point
            x = convertX(m_radiusIncrems * stack + m_radiusIncrems, -posNeg*(sliceAng));
            z = convertZ(m_radiusIncrems * stack + m_radiusIncrems, -posNeg*(sliceAng));
            insertPoint(x, myY, z);
            insertPoint(0, posNeg, 0);

            // Bottom right point
            x = convertX(m_radiusIncrems * stack + m_radiusIncrems, -posNeg*(sliceAng + m_sliceAngles));
            z = convertZ(m_radiusIncrems * stack + m_radiusIncrems, -posNeg*(sliceAng + m_sliceAngles));
            insertPoint(x, myY, z);
            insertPoint(0, posNeg, 0);
        }
    }
}

void cone::makeTopCone(float maxY)
{
    float topRad = m_radiusIncrems;
    float x, y, z;
    for (float currAngle = 0; currAngle < (2 * M_PI); currAngle += m_sliceAngles) {
        // Top point;
        insertPoint(0, maxY, 0);
        insertPoint(2/sqrt(5) * cos(-1*(currAngle + m_sliceAngles/2)), 1/sqrt(5), 2/sqrt(5) * sin(-1*(currAngle + m_sliceAngles/2)));

        // Bottom left point;
        x = convertX(topRad, -1*currAngle);
        y = maxY - m_stackIncrems;
        z = convertZ(topRad, -1*currAngle);
        insertPoint(x, y, z);
        insertPoint(2/sqrt(5) * cos(-1*currAngle), 1/sqrt(5), 2/sqrt(5) * sin(-1*currAngle));

        // Bottom right points;
        x = convertX(topRad, -1*(currAngle + m_sliceAngles));
        y = maxY - m_stackIncrems;
        z = convertZ(topRad, -1*(currAngle + m_sliceAngles));
        insertPoint(x, y, z);
        insertPoint(2/sqrt(5) * cos(-1*(currAngle + m_sliceAngles)), 1/sqrt(5), 2/sqrt(5) * sin(-1*(currAngle + m_sliceAngles)));
    }
}

void cone::makeBottomCone()
{
    float x, y, z, currRad, startingY;
    currRad = m_radius - m_radiusIncrems;
    // Loop through m_stacks
    for (int stack = 0; stack < m_stacks - 1; stack++) {
        // Looping through m_slices
        for (float sliceAng = 0; sliceAng < (2 * M_PI); sliceAng += m_sliceAngles) {
            startingY = -0.5 + m_stackIncrems * stack;
            // FIRST TRIANGLE
            // Top left point
            x = convertX(currRad, -1*sliceAng);
            y = startingY + m_stackIncrems;
            z = convertZ(currRad, -1*sliceAng);
            insertPoint(x, y, z);
            insertPoint(2/sqrt(5) * cos(-1*sliceAng), 1/sqrt(5), 2/sqrt(5) * sin(-1*sliceAng));

            // Bottom left point
            x = convertX(currRad + m_radiusIncrems, -1*sliceAng);
            y = startingY;
            z = convertZ(currRad + m_radiusIncrems, -1*sliceAng);
            insertPoint(x, y, z);
            insertPoint(2/sqrt(5) * cos(-1*sliceAng), 1/sqrt(5), 2/sqrt(5) * sin(-1*sliceAng));

            // Top right point
            x = convertX(currRad, -1*(sliceAng + m_sliceAngles));
            y = startingY + m_stackIncrems;
            z = convertZ(currRad, -1*(sliceAng + m_sliceAngles));
            insertPoint(x, y, z);
            insertPoint(2/sqrt(5) * cos(-1*(sliceAng + m_sliceAngles)), 1/sqrt(5), 2/sqrt(5) * sin(-1*(sliceAng + m_sliceAngles)));

            // SECOND TRIANGLE
            // Top right point
            x = convertX(currRad, -1*(sliceAng + m_sliceAngles));
            y = startingY + m_stackIncrems;
            z = convertZ(currRad, -1*(sliceAng + m_sliceAngles));
            insertPoint(x, y, z);
            insertPoint(2/sqrt(5) * cos(-1*(sliceAng + m_sliceAngles)), 1/sqrt(5), 2/sqrt(5) * sin(-1*(sliceAng + m_sliceAngles)));

            // Bottom left point
            x = convertX(currRad + m_radiusIncrems, -1*sliceAng);
            y = startingY;
            z = convertZ(currRad + m_radiusIncrems, -1*sliceAng);
            insertPoint(x, y, z);
            insertPoint(2/sqrt(5) * cos(-1*sliceAng), 1/sqrt(5), 2/sqrt(5) * sin(-1*sliceAng));

            // Bottom right point
            x = convertX(currRad + m_radiusIncrems, -1*(sliceAng + m_sliceAngles));
            y = startingY;
            z = convertZ(currRad + m_radiusIncrems, -1*(sliceAng + m_sliceAngles));
            insertPoint(x, y, z);
            insertPoint(2/sqrt(5) * cos(-1*(sliceAng + m_sliceAngles)), 1/sqrt(5), 2/sqrt(5) * sin(-1*(sliceAng + m_sliceAngles)));
        }
        currRad -= m_radiusIncrems;
    }
}

float cone::convertX(float r, float theta)
{
    return r * cos(theta);
}

float cone::convertZ(float r, float theta)
{
    return r * sin(theta);
}
