#ifndef RAY_H
#define RAY_H

#include <CS123Common.h>

class Ray
{
public:
    Ray(int row, int col, float direction);

private:


    glm::vec2 m_position;

    // Pixel coordinates for this Ray.
    int m_myRow, m_myCol;

    // Direction for current ray.
    float m_direction;
};

#endif // RAY_H
