William Truong
wtruong
11/15/15
CS1230: Intersect README

********************
*   DESIGN SPECS   *
********************

In Canvas2D, renderImage() handles looping through the canvas and calculating the proper pFilm and pWorld coordinates. This function also handles updating the canvas colors to that representative of the scene we just ray-traced. 

I've defined a struct called CS123Ray in CS123SceneData.h that maintains 2 glm::vec4s for the origin point of the ray and the direction the ray is shot in. 

As we loop through the canvas in Canvas2D::renderImage(), a ray is constructed with the proper origin point and direction. RayScene::shootRay(ray) is then called to shoot the ray and call subfunctions that perform all the calculations for intersections, normals at the intersection, as well as calculating the lighting equation to return the proper color to be placed on the canvas.

RayScene.cpp maintains several subfunctions (as mentioned above) that perform numerous calculations. I've defined 4 functions (implicitCube, implicitCone, implicitCylinder, implicitSphere) that take in a point and direction of the rya and checks if the ray intersects with our implicit shapes. These functions return a t value in which the ray intersected the shape. implicitCone and implicitCylinder return a vec2, where the first value is the t-value, and the second value is a flag for if the closest intersection for the ray was the cap (this is used to compute the normals at this intersection).

I created a function called intersectPlane() that is called to check if a ray intersects with the caps of cone/cylinder or the faces of a cube.

getNormal() calculates the normal for a certain intersection point for a given shape and returns it (used in lighting equation).

finalIntensity() calculates the lighting equation for the proper output of color to be painted on the canvas.

******************
*   KNOWN BUGS   *
******************

When rendering cubes, odd pixelations occur (ex. render ctc_isect.xml to see what I'm talking about). For some reason, random black pixels appear on the top and bottom faces of my cube renders. Not sure if this is an issue with floating point precision or something.