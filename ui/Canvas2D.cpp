/**
 * @file Canvas2D.cpp
 *
 * CS123 2-dimensional canvas. Contains support code necessary for Brush, Filter, Intersect, and
 * Ray.
 *
 * YOU WILL NEED TO FILL THIS IN!
 *
 */

// For your convenience, a few headers are included for you.
#include <math.h>
#include <assert.h>
#include <unistd.h>
#include <iostream>
#include "Canvas2D.h"
#include "Canvas3D.h"
#include "Settings.h"
#include "RayScene.h"
#include "RaySceneRunnable.h"

#include <QPainter>
#include <QCoreApplication>

#include <QThreadPool>
#include <QRunnable>

Canvas2D::Canvas2D():
    m_brush(nullptr)
{
    // @TODO: Initialize any pointers in this class here.
    m_scene = NULL;
    this->settingsChanged();
}

Canvas2D::~Canvas2D()
{
    // @TODO: Be sure to release all memory that you allocate.
    delete m_scene;

}

// This is called when the canvas size is changed. You can change the canvas size by calling
// resize(...). You probably won't need to fill this in, but you can if you want to.
void Canvas2D::notifySizeChanged(int w, int h) {

}

void Canvas2D::paintEvent(QPaintEvent *e) {
    // You probably won't need to fill this in, but you can if you want to override any painting
    // events for the 2D canvas. For now, we simply call the superclass.

    SupportCanvas2D::paintEvent(e);

}

// ********************************************************************************************
// ** BRUSH
// ********************************************************************************************

void Canvas2D::mouseDown(int x, int y)
{
    // @TODO: [BRUSH] Mouse interaction for brush. You will probably want to create a separate
    //        class for each of your brushes. Remember that you can use the static Settings
    //        object to get the currently selected brush and its parameters.

    bool fixAlphaBlending = settings.fixAlphaBlending; // for extra/half credit

    // Call pickUpPaint when the user clicks down; function is only defined in SmudgeBrush.cpp.
    // pickUpPaint is left empty in Brush.cpp.
    m_brush->pickUpPaint(x, y, this);

    // Paint when the user clicks down.
    m_brush->paintOnce(x, y, this);
}

void Canvas2D::mouseDragged(int x, int y)
{
    // TODO: [BRUSH] Mouse interaction for Brush.

    // When a user drags the mouse, paint.
    m_brush->paintOnce(x, y, this);
}

void Canvas2D::mouseUp(int x, int y)
{
    // TODO: [BRUSH] Mouse interaction for Brush.

}



// ********************************************************************************************
// ** FILTER
// ********************************************************************************************

void Canvas2D::filterImage()
{
    // TODO: [FILTER] Filter the image. Some example code to get the filter type is provided below.

    switch (settings.filterType) {
    case FILTER_INVERT:
        m_filter.reset(new InverseFilter(this));
        m_filter->scanImage();
        break;
    case FILTER_GRAYSCALE:
        m_filter.reset(new GrayscaleFilter(this));
        m_filter->scanImage();
        break;
    case FILTER_EDGE_DETECT:
        // Gray scale the image first.
        m_filter.reset(new GrayscaleFilter(this));
        m_filter->scanImage();
        // Then apply edge detect.
        m_filter.reset(new EdgeDetectFilter(this));
        m_filter->scanImage();
        break;
    case FILTER_BLUR:
        m_filter.reset(new BlurFilter(this));
        m_filter->scanImage();
        break;
    case FILTER_SCALE:
        m_filter.reset(new ScaleFilter(this));
        m_filter->scanImage();
        break;
    case FILTER_SPECIAL_1:
        m_filter.reset(new SharpenFilter(this));
        m_filter->scanImage();
        break;
    case FILTER_SPECIAL_2:
        m_filter.reset(new SharpenFilter(this));
        m_filter->scanImage();
        break;
    case FILTER_SPECIAL_3:
        m_filter.reset(new SharpenFilter(this));
        m_filter->scanImage();
        break;
    }
    update();
}

void Canvas2D::setScene(RayScene *scene)
{
    delete m_scene;
    m_scene = scene;
}


void Canvas2D::renderImage(Camera *camera, int width, int height)
{
    if (m_scene)
    {
//        m_scene->filterImage(this, camera, width, height, 0, height);

//        m_scene->

        this->resize(width, height);
        int rowOffset = ceil(height / 8);
        RaySceneRunnable *rayRun1 = new RaySceneRunnable(m_scene, this, camera, width, height, 0, rowOffset);
        RaySceneRunnable *rayRun2 = new RaySceneRunnable(m_scene, this, camera, width, height, rowOffset, rowOffset * 2);
        RaySceneRunnable *rayRun3 = new RaySceneRunnable(m_scene, this, camera, width, height, rowOffset * 2, rowOffset * 3);
        RaySceneRunnable *rayRun4 = new RaySceneRunnable(m_scene, this, camera, width, height, rowOffset * 3, rowOffset * 4);
        RaySceneRunnable *rayRun5 = new RaySceneRunnable(m_scene, this, camera, width, height, rowOffset * 4, rowOffset * 5);
        RaySceneRunnable *rayRun6 = new RaySceneRunnable(m_scene, this, camera, width, height, rowOffset * 5, rowOffset * 6);
        RaySceneRunnable *rayRun7 = new RaySceneRunnable(m_scene, this, camera, width, height, rowOffset * 6, rowOffset * 7);
        RaySceneRunnable *rayRun8 = new RaySceneRunnable(m_scene, this, camera, width, height, rowOffset * 7, height);
        QThreadPool::globalInstance()->start(rayRun1);
        QThreadPool::globalInstance()->start(rayRun2);
        QThreadPool::globalInstance()->start(rayRun3);
        QThreadPool::globalInstance()->start(rayRun4);
        QThreadPool::globalInstance()->start(rayRun5);
        QThreadPool::globalInstance()->start(rayRun6);
        QThreadPool::globalInstance()->start(rayRun7);
        QThreadPool::globalInstance()->start(rayRun8);
    }
}

void Canvas2D::cancelRender()
{
    // TODO: cancel the raytracer (optional)
}



void Canvas2D::settingsChanged() {

    // TODO: Process changes to the application settings.
    int currentBrush = settings.brushType;
    int currentRadius = settings.brushRadius;

    BGRA currentColor;
    currentColor.b = settings.brushBlue;
    currentColor.g = settings.brushGreen;
    currentColor.r = settings.brushRed;
    currentColor.a = 255;

    int currentFlow = settings.brushAlpha;
    // You're going to need to leave the alpha value on the canvas itself at 255, but you will
    // need to use the actual alpha value to compute the new color of the pixel

    switch(currentBrush) {
        // Solid
        case 0:
            m_brush.reset(new ConstantBrush(currentColor, currentFlow, currentRadius));
            break;
        // Linear
        case 1:
            m_brush.reset(new LinearBrush(currentColor, currentFlow, currentRadius));
            break;
        // Quadratic
        case 2:
            m_brush.reset(new QuadraticBrush(currentColor, currentFlow, currentRadius));
            break;
        case 3:
            m_brush.reset(new SmudgeBrush(currentColor, currentFlow, currentRadius));
            break;
        default:
            m_brush.reset(new ConstantBrush(currentColor, currentFlow, currentRadius));
            break;
        }
}
