William Truong
wtruong
9/17/15
CS1230: Filter README

********************
*   DESIGN SPECS   *
********************

For this project, I created a super class Filter where each of my filters subclassed from. The superclass has a function called get1DIndex() that, given the width, row and column indices of a 2D array, returns the corresponding 1D index. scanImage() is a virtual function that scans the image and applies a filter for whichever option was chosen from the GUI. 

scanImage() is overwritten in the filters that require convolution (all excluding Grayscale and Invert). Those subclasses implement their own individual functions that convolve the image appropriately (see in-line comments per class).

The special filter I created was sharpen. This was done by applying a 2D convolution on the image on one pass, using a matrix that emphasizes the energy value of the current pixel and applied a negative emphasis on the immediate adjacent pixels.

******************
*   KNOWN BUGS   *
******************

No known bugs.